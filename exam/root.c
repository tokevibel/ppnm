#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"
//#include<stdio.h>


int newton_with_jacobian(
        void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
        gsl_vector* x, double epsilon){

	gsl_vector* fx = gsl_vector_alloc(x->size);
	gsl_matrix* J = gsl_matrix_alloc(x->size, x->size);
	gsl_matrix* R = gsl_matrix_alloc(J->size1, J->size2);
	gsl_vector* dx = gsl_vector_alloc(x->size);

	f(x, fx, J);
	double lambda;
	double lambda_min = 1.0/65.0;
	gsl_vector* fx_temp = gsl_vector_alloc(fx->size);
	gsl_vector* x_temp = gsl_vector_alloc(x->size);
	int iter = 0;
	int iter_max = 30;
	while(gsl_blas_dnrm2(fx)>epsilon && iter < iter_max){
		iter++;
		f(x, fx, J);
		qr_gs_decomp(J, R);

		gsl_vector_scale(fx, -1.0);
		qr_gs_solve(J, R, fx, dx);

		lambda = 1;
		gsl_vector_memcpy(x_temp, x);
		gsl_blas_daxpy(lambda, dx, x_temp);
		f(x_temp, fx_temp, J);
		while(gsl_blas_dnrm2(fx_temp) >= (1-lambda/2.0)*gsl_blas_dnrm2(fx) &&
				lambda > lambda_min){

			lambda /=2;
			gsl_blas_daxpy(-lambda, dx, x_temp);
			f(x_temp, fx_temp, J);
		}
		gsl_vector_memcpy(x, x_temp);
		gsl_vector_memcpy(fx, fx_temp);
		//if(iter>10) printf("%.22g\n",gsl_vector_get(x,0));
	}
	if(iter == iter_max)
		return 1;

	gsl_vector_free(fx);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(dx);
	gsl_vector_free(fx_temp);
	gsl_vector_free(x_temp);

return 0;
}
