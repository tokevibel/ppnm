#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>


int newton_with_jacobian(
        void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
        gsl_vector* x, double epsilon);

void symmetric_rc_update(gsl_vector* d, gsl_vector* u, int p, gsl_vector* d_update){
	int n = d->size;

	//defining function given secular equation and its derivative as a nested function
	//enabling n in to be changed automatically and avoid to change function newton_with_jacobian
	void secular_eq(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
		double lambda = gsl_vector_get(x, 0);
		double sum_f = 0, sum_df = 0;
		for(int i = 0; i<n; i++){if(i != p){
			sum_f += pow(gsl_vector_get(u, i), 2) / (gsl_vector_get(d, i)-lambda);
			sum_df += pow(gsl_vector_get(u, i), 2) * pow((gsl_vector_get(d, i)-lambda), -2);
			}
		}
		double f = -(gsl_vector_get(d, p)-lambda) + sum_f;
		gsl_vector_set(fx, 0, f);
		double df = 1 + sum_df;
		gsl_matrix_set(J, 0, 0, df);
	}

	gsl_vector* lambda = gsl_vector_alloc(1);
	double error = 1e-7; 	//maksimum error for root-finding
	double delta = 1e-7; 	//tolerance when checking if two numbers are identical
	double guess; 		//initial guess, when calling root-finding function
	double result; 		//result of root-finding
	double eps = 1e-3; 	//small number used if for new start parameter if root is found in wrong interval
	int flag;		//variable, which deterines if root was found by root-finding function

	for(int i=0; i<n; i++){
	  // If u_i = 0 the i'th eigenvalue is not changed
	  if(fabs(gsl_vector_get(u, i)) < delta && i != p){
	    gsl_vector_set(d_update, i, gsl_vector_get(d, i));
	  }else{
	  if(i<p){
	    if(i != 0){
	      //Checking for degeneracy
	      if(gsl_vector_get(d, i)-gsl_vector_get(d, i-1) < delta){//by assumption d_i>=d_i-1
	        gsl_vector_set(d_update, i, gsl_vector_get(d, i));
	      }else{
		guess = 0.5*(gsl_vector_get(d, i-1)+gsl_vector_get(d, i));
	        gsl_vector_set(lambda, 0, guess);
 	        flag = newton_with_jacobian(secular_eq, lambda, error);
	        result = gsl_vector_get(lambda, 0);
                //Check if root is in correct interval. Else: try a new guess
		if(result >= gsl_vector_get(d, i-1) && result <= gsl_vector_get(d, i)){
	          gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
		  if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		}else{
		  if(result < gsl_vector_get(d, i-1)){
		    guess = gsl_vector_get(d, i-1)+eps;
		  }else{
		    guess = gsl_vector_get(d, i)-eps;
		  }
		  gsl_vector_set(lambda,0, guess);
 	      	  flag = newton_with_jacobian(secular_eq, lambda, error);
	      	  result = gsl_vector_get(lambda, 0);
		  if(result >= gsl_vector_get(d, i-1) && result <= gsl_vector_get(d, i)){
		    gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
		    if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		  }else{
		    printf("Could not find eigenvalue with index=%d\n", i);
		    gsl_vector_set(d_update, i, NAN);
		  }
		}
              }
            }else{// i ==0
	      guess = 1.5*gsl_vector_get(d, 0)-0.5*gsl_vector_get(d, 1);
	      gsl_vector_set(lambda, 0, guess);
 	      flag = newton_with_jacobian(secular_eq, lambda, error);
	      result = gsl_vector_get(lambda, 0);
              //Check if root is in correct interval. Else: try a new guess
	      if(result <= gsl_vector_get(d, 0)){
	        gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
	        if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
	      }else{
		guess = gsl_vector_get(d, 0)-eps;
		gsl_vector_set(lambda, 0, guess);
 	      	flag = newton_with_jacobian(secular_eq, lambda, error);
	      	result = gsl_vector_get(lambda, 0);
		if(result <= gsl_vector_get(d, 0)){
		  gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
		  if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		}else{
		  printf("Could not find eigenvalue with index=%d\n", i);
		  gsl_vector_set(d_update, i, NAN);
		}
	      }
	    }

	  }else if(i==p){
	    if(i != 0 && i != n-1){// p is neither first or last index
	      //Checking for threefold degeneracy
      	      if(gsl_vector_get(d, i+1)-gsl_vector_get(d, i-1) < delta){
	        gsl_vector_set(d_update, i, gsl_vector_get(d, i));
	      }else{
	        guess = 0.5*(gsl_vector_get(d, i-1)+gsl_vector_get(d, i+1));
	        gsl_vector_set(lambda, 0, guess);
 	        flag = newton_with_jacobian(secular_eq, lambda, error);
	        result = gsl_vector_get(lambda, 0);
                //Check if root is in correct interval. Else: try a new guess
	        if(result >= gsl_vector_get(d, i-1) && result <= gsl_vector_get(d, i+1)){
	          gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
       		  if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		}else{
		  if(result < gsl_vector_get(d, i-1)){
		    guess = gsl_vector_get(d, i-1)+eps;
		  }else{
		    guess = gsl_vector_get(d, i+1)-eps;
		  }
		  gsl_vector_set(lambda,0, guess);
 	      	  flag = newton_with_jacobian(secular_eq, lambda, error);
	      	  result = gsl_vector_get(lambda, 0);
		  if(result >= gsl_vector_get(d, i-1) && result <= gsl_vector_get(d,i+1)){
		    gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
       		    if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		  }else{
		    printf("Could not find eigenvalue with index=%d\n", i);
		    gsl_vector_set(d_update, i, NAN);
		  }
		}
              }
	    }else if(i==0){
	      guess = 1.5*gsl_vector_get(d, 1)-0.5*gsl_vector_get(d, 2);
	      gsl_vector_set(lambda, 0, guess);
 	      flag = newton_with_jacobian(secular_eq, lambda, error);
  	      result = gsl_vector_get(lambda, 0);
              //Check if root is in correct interval. Else: try a new guess
	      if(result <= gsl_vector_get(d, 1)){
	        gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
   		if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
	      }else{
	        guess = gsl_vector_get(d, 1)-eps;
		gsl_vector_set(lambda, 0, guess);
 	      	flag = newton_with_jacobian(secular_eq, lambda, error);
	      	result = gsl_vector_get(lambda, 0);
		if(result <= gsl_vector_get(d, 1)){
		  gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
       		  if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		}else{
		  printf("Could not find eigenvalue with index=%d\n", i);
		  gsl_vector_set(d_update, i, NAN);
		}
              }
	    }else{//i = n-1
	      guess = 1.5*gsl_vector_get(d, n-2)-0.5*gsl_vector_get(d, n-3); //don't work for n=2 matrix
	      gsl_vector_set(lambda,0, guess);
 	      flag = newton_with_jacobian(secular_eq, lambda, error);
	      result = gsl_vector_get(lambda, 0);
              //Check if root is in correct interval. Else: try a new guess
	      if(result >= gsl_vector_get(d, i-1)){
	        gsl_vector_set(d_update, i, gsl_vector_get(lambda,0));
		if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
	      }else{
		guess = gsl_vector_get(d, i-1)+eps;
		gsl_vector_set(lambda, 0, guess);
 	      	flag = newton_with_jacobian(secular_eq, lambda, error);
	      	result = gsl_vector_get(lambda, 0);
		if(result >= gsl_vector_get(d, i-1)){
		  gsl_vector_set(d_update, i, gsl_vector_get(lambda, 0));
       		  if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		}else{
		  printf("Could not find eigenvalue with index=%d\n", i);
		  gsl_vector_set(d_update, i, NAN);
		}
              }
            }

	  }else{// i>p
	    if(i != n-1){
	      //Checking for degeneracy
      	      if(gsl_vector_get(d, i+1)-gsl_vector_get(d,i) < delta){
	        gsl_vector_set(d_update, i, gsl_vector_get(d,i));
	      }else{
       	        guess = 0.5*(gsl_vector_get(d, i)+gsl_vector_get(d, i+1));
	        gsl_vector_set(lambda,0, guess);
 	        flag = newton_with_jacobian(secular_eq, lambda, error);
	        result = gsl_vector_get(lambda,0);
                //Check if root is in correct interval. Else: try a new guess
	        if(result >= gsl_vector_get(d,i) && result <= gsl_vector_get(d,i+1)){
	          gsl_vector_set(d_update, i, gsl_vector_get(lambda,0));
       		  if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
	        }else{
		  if(result < gsl_vector_get(d, i)){
		    guess = gsl_vector_get(d, i) + eps;
		  }else{
		    guess = gsl_vector_get(d, i+1) - eps;
		  }
		  gsl_vector_set(lambda,0, guess);
 	      	  flag = newton_with_jacobian(secular_eq, lambda, error);
	      	  result = gsl_vector_get(lambda,0);
		  if(result >= gsl_vector_get(d,i) && result <= gsl_vector_get(d,i+1)){
		    gsl_vector_set(d_update, i, gsl_vector_get(lambda,0));
       		    if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		  }else{
		    printf("Could not find eigenvalue with index=%d\n",i);
		    gsl_vector_set(d_update, i, NAN);
		  }
                }
	      }
	    }else{//i ==n-1
      	      guess = 1.5*gsl_vector_get(d, n-1)-0.5*gsl_vector_get(d, n-2);
	      gsl_vector_set(lambda,0, guess);
 	      flag = newton_with_jacobian(secular_eq, lambda, error);
	      result = gsl_vector_get(lambda,0);
              //Check if root is in correct interval. Else: try a new guess
	      if(result >= gsl_vector_get(d, n-1)){
	        gsl_vector_set(d_update, i, gsl_vector_get(lambda,0));
      		if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
	      }else{
 	        guess = gsl_vector_get(d, n-1)+eps;
		gsl_vector_set(lambda,0, guess);
 	      	flag = newton_with_jacobian(secular_eq, lambda, error);
	      	result = gsl_vector_get(lambda,0);
		if(result >= gsl_vector_get(d, n-1)){
		  gsl_vector_set(d_update, i, gsl_vector_get(lambda,0));
       		  if(flag==1) printf("Could not find eigenvalue with index=%d within the required error\n", i);
		}else{
		  printf("Could not find eigenvalue with index=%d\n",i);
		  gsl_vector_set(d_update, i, NAN);
		}
              }
	    }
	  }}

	}// big for-loop

	gsl_vector_free(lambda);

}
