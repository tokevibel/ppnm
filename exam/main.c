#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<limits.h>
#include<time.h>
#include<math.h>

void symmetric_rc_update(gsl_vector* d, gsl_vector* u, int p, gsl_vector* d_update);
int jacobi_cyclic(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

void matrix_print(FILE* output, const char *s, gsl_matrix* A);
void vector_print(FILE* output, const char *s, gsl_vector* v);

int main(){
	int p = 1; // Choosing p in e(p) (cf. eq. 3.15)
	int start_t_sym, end_t_sym, start_t_jacobi, end_t_jacobi; //variables for measurement of execution time
	double total_t_sym, total_t_jacobi;

	//Printing to log
	printf("\nError messages for matrices of size\n");

	//data to check how the program scale with n (dimension of matrix)
	FILE* scale = fopen("scale.data", "w");
	for(int n=4; n<205; n+=50){
		printf("n = %d:\n", n);
		gsl_vector* d = gsl_vector_alloc(n); //variables for symmetric_rc_update
		gsl_vector* u = gsl_vector_calloc(n);
		gsl_vector* d_update = gsl_vector_alloc(n);
		gsl_matrix* D = gsl_matrix_calloc(n, n);//variables for jacobi_cyclic
		gsl_matrix* A = gsl_matrix_calloc(n, n);
		gsl_vector* e = gsl_vector_alloc(n);
		gsl_matrix* V = gsl_matrix_alloc(n,n);

		double ui;
		//Define d, u, D, and A
		for(int i=0; i<n; i++){
			gsl_vector_set(d, i, i-2);
			gsl_matrix_set(D, i, i, i-2);
			gsl_matrix_set(A, i, i, i-2);
			if(i != p){
				ui = ((double)rand()/INT_MAX-0.5)*2*n;
				gsl_vector_set(u, i, ui);
				gsl_matrix_set(A, i, p, ui);
				gsl_matrix_set(A, p, i, ui);
			}
		}

		//Calculate new eigenvalues and measure time
		start_t_sym = clock();
		symmetric_rc_update(d, u, p, d_update);
		end_t_sym = clock();
		total_t_sym = (double)(end_t_sym-start_t_sym)/CLOCKS_PER_SEC;

		//Compare with Jacobi diagonalization
		start_t_jacobi = clock();
		jacobi_cyclic(A, e, V);
		end_t_jacobi = clock();
		total_t_jacobi = (double)(end_t_jacobi-start_t_jacobi)/CLOCKS_PER_SEC;

		fprintf(scale, "%d %g %g %g %g\n", n, total_t_sym, total_t_jacobi, 7e-6*n*n, 8e-7*n*n*n);

		// Creating plotting data for secular equation and printing example of solution
		if(n==4){
			FILE* data = fopen("plot.data","w");
			double sum_f;
			for(double x=-10; x<10; x+=0.001){
				sum_f = 0;
				for(int i = 0; i<n; i++){if(i != p){
					sum_f += pow(gsl_vector_get(u, i),2)/(gsl_vector_get(d,i)-x);
				}}
				double fx = -(gsl_vector_get(d,p)-x) + sum_f;
		                fprintf(data,"%g %g\n",x, fx);
			}
		        fclose(data);

			//Reconstruct A
			FILE* out = fopen("out.txt", "w");
			for(int i = 0; i<n; i++)for(int j=i+1; j<n; j++){
				gsl_matrix_set(A, i, j, gsl_matrix_get(A, j, i));
			}
			matrix_print(out, "\nFor a matrix A =", A);
			fprintf(out, "decomposed according to A = D + e(p)u^T + ue(p)T with p = %d,\n",p);
			matrix_print(out, "D=", D);
			vector_print(out, "and u=", u);

			vector_print(out, "the updated eigenvalues are calculated to be\n", d_update);
			fprintf(out, "Using Jacobi diagonalization with cyclic sweeps the eigenvalues are\n");
			vector_print(out, "", e);
			fclose(out);

        	}

		gsl_matrix_free(D);
		gsl_matrix_free(A);
		gsl_vector_free(d);
		gsl_vector_free(d_update);
		gsl_vector_free(u);
		gsl_vector_free(e);
		gsl_matrix_free(V);
	}

	fclose(scale);
return 0;
}


void vector_print(FILE* output, const char *s, gsl_vector* v){
	fprintf(output,"%s", s);
	for(int i = 0; i < v->size; i++)
		fprintf(output, "%8.3g ", gsl_vector_get(v, i));
	fprintf(output, "\n");
}

void matrix_print(FILE* output, const char *s, gsl_matrix* A){
	fprintf(output, "%s\n", s);
	for(int i = 0; i < A->size1; i++){
		for(int j = 0; j < A->size2; j++){
			fprintf(output, "%8.3g ", gsl_matrix_get(A, i, j));
		}
		fprintf(output, "\n");
	}
}
