\documentclass[onecolumn]{article}
\usepackage{graphicx}
\usepackage{amsmath}
\begin{document}
\author{Toke Vibel, student-number: 201803159}
\date{\today}
\title{Report on examination project, number 13\\\large{Symmetric row/column update of a size-n symmetric eigenvalue problem}}
\maketitle

\section{Introduction}
This report describes the problem of diagonalizing a matrix in form
\begin{align}
    \mathrm{\mathbf{A}} = \mathrm{\mathbf{D}} + \mathrm{\mathbf{e}}_p 
    \mathrm{\mathbf{u}}^T + \mathrm{\mathbf{u}} \mathrm{\mathbf{e}}_p^T  = 
    \begin{bmatrix} 
    d_0 & \dots & u_0 & \dots & 0\\
    \vdots & \ddots & \vdots & \ddots & \vdots \\
    u_0 & \dots & d_p & \dots & u_{n-1}  \\
    \vdots & \ddots & \vdots   & \ddots & \vdots\\
    0 & \dots & u_{n-1} & \dots & d_{n-1}
    \end{bmatrix}\!,
    \label{eq:main}
\end{align}
where \textbf{D} is a $n\times n$ diagonal matrix with diagonal elements 
$\{d_k, k=0,...,n-1 \}$, $\mathrm{\mathbf{u}}$ is a given column-vector, 
and the vector $\mathrm{\mathbf{e}}_p$ with components $e_i = \delta_{ip}$ 
is a unit vector in the direction $p$ where $0 \leq p \leq n-1$. 

Generally, a non-zero vector \textbf{v} is called an \textit{eigenvector} 
of a matrix \textbf{A} with the eigenvalue $\lambda$, if \cite{bib:main}
\begin{align}
  \mathrm{\mathbf{Av}}=\lambda \mathrm{\mathbf{v}}.
\end{align}
If \textbf{A} is real and symmetric it is diagonalizable with real 
eigenvalues, and there exits an orthogonal matrix 
$\mathrm{\mathbf{V}}=\{\mathrm{\mathbf{v}}_0,\mathrm{\mathbf{v}}_1,...,\mathrm{\mathbf{v}}_{n-1}\}$ 
consisting of the ordered eigenvectors such that
\begin{align}
    \mathrm{\mathbf{D}} = \mathrm{\mathbf{VAV}}^{-1} 
    =\begin{bmatrix} \lambda_0 & 0 & \dots & 0 \\
    0 & \lambda_1 &  & \vdots \\
    \vdots &  & \ddots &  \\
    0 & \dots &   & \lambda_{n-1} 
    \end{bmatrix}
\end{align}
is a diagonal matrix. $\{\lambda_0, \lambda_1,..., \lambda_{n-1 }\}$ are 
the $n$ eigenvalues. Matrix diagonalization refers to the problem of 
finding the eigenvalues. 

Since similarity transformations (rotations) do not influence the 
eigenvalues and eigenvectors general methods exit, where a sequence of 
rotations bring a matrix to diagonal form. One such method is called the 
\textit{Jacobi eigenvalue algorithm}. A certain version of this algorithm 
is called the cyclic method where so called Jacobi rotations are applied 
in a strict order. The number of operations needed in this method scales 
as $O(n^3)$. 

If, however, the matrix to be diagonalized have a specific form as fx in 
eq. (\ref{eq:main}), there exit an algorithm offering a more efficient 
calculation of the \textit{updated} eigenvalues. The algorithm for matrices 
on the form of eq. (\ref{eq:main}) is called symmetric row/column update 
and is a special case of rank-2 update. It can be shown that the updated 
eigenvalues of a symmetric row/column update are determined as the root of 
following secular equation 
\begin{align}
    f(\lambda) = (d_p - \lambda) + \sum_{k \neq p }^{n-1} 
    \frac{u_k^2}{d_k-\lambda}.
    \label{eq:secular}
\end{align}
Finding the roots of this equations can be done in $O(n^2)$ operation, as 
will be confirmed in this report.  

A program, called \texttt{jacobi\_cyclic}, using the Jacobi algorithm was 
written in exercise "matrix diagonalization" part A., and will be used for 
comparison with the algorithm written in this examination project.

\section{Method}
The primary function of this project has following form:
\begin{quote}
\texttt{void symmetric\_rc\_update(gsl\_vector* d, gsl\_vector* u,\\
int p, gsl\_vector* d\_update)}. 
\end{quote}
It takes as input a vector \texttt{d} containing the eigenvalues 
$\{d_k, k=0,...,n-1 \}$ where $d_k \leq d_{k+1}$ and $n \geq 3 $ , the 
arbitrary vector \texttt{u} (with $u_p = 0$), and the index $p$. It saves 
the updated eigenvalue in the vector \texttt{d\_update}.

From eq. (\ref{eq:secular}) which is visualized in fig. \ref{fig:secular} 
it is clear that $f(-\infty) =- \infty$ and $f(\infty) = \infty$. 
Furthermore, for $\lambda$ being infinitesimally less than a 
$d_k$ $f(d_k-d\lambda) = \infty$ and for lambda being infinitesimally 
greater than a $d_k$ $f(d_k+d\lambda) = -\infty$. This means that the 
$\lambda$-axis is divided in $n$ parts or intervals, which all contain 
one root.

\begin{figure}[h!]
\input{plot_secular.tex}
\caption{Visualization of the secular equation in eq. (\ref{eq:secular}) 
for a matrix of size $n =4$, $p=1$, $d_0 = -2, d_1 = -1, d_2 = 0, d_3 = 1$, 
and $u_0 = 2.72, u_1 = 0, u_2 = -0.845, u_3 = 2.26$.}
\label{fig:secular}
\end{figure}

To find the root of eq. (\ref{eq:secular}), the self-written routine 
\texttt{newton\_with\_jacobian} from exercise "Root Finding", which make 
use of self-written routines for solving linear equation, is used. 
\texttt{newton\_with\_jacobian} is a multi-dimensional routine despite 
that the secular equation is one-dimensional. The routine is ask to find 
a root within the maximum deviation given in the variable \texttt{error}, 
which is set to 1e-7.

To make sure that one and one root only is found in each interval a tree 
of \texttt{if}-statements place the initial guesses for the root-finding 
routine in the right interval. This is shown in following table.

\begin{center}
  \begin{tabular}{ | l | l | l |}
    \hline
    \texttt{if(k < p)}& \texttt{if(k==p)} & \texttt{if(k > p)} \\ \hline
    \quad \texttt{if(k!=0)}  & \quad \texttt{if(k!=0 \&\& k!=n-1)} & \quad 
    \texttt{if(k!=n-1)} \\ 
    \quad \quad $\mathtt{d_{k-1}\leq \lambda_k\leq d_k}$ & 
    \quad \quad $\mathtt{d_{k-1}\leq \lambda_k\leq d_{k+1}}$& 
    \quad \quad $\mathtt{d_{k}\leq \lambda_k\leq d_{k+1}}$ \\ \hline
    \quad \texttt{if(k==0)} & \quad \texttt{if(k==0)} &
    \quad \texttt{if(k==n-1)} \\
    \quad \quad $\mathtt{\lambda_0\leq d_0}$ & 
    \quad \quad $\mathtt{\lambda_0\leq d_1}$& 
    \quad \quad $\mathtt{d_{n-1}\leq \lambda_{n-1}}$ \\ 
    \hline
    & \quad \texttt{if(k==n-1)}  & \\
    & \quad \quad $\mathtt{d_{n-2} \leq \lambda_{n-1}}$& \\ 
    \hline
  \end{tabular}
\end{center}

Additionally, when the \texttt{newton\_with\_jacobian} function returns 
a root it is checked that the root is still inside the correct interval, 
since it often happens that the root-finding routine jump to another 
interval. If this happens the root-finding routine is repeated. If the 
root is higher than the upper boundary of the interval, the new initial 
value is set to $(\mathrm{upper\: boundary - }\mathtt{eps})$, where 
\texttt{eps} (epsilon) is a small number - in this project it is set to 
1e-3. If the root is less than the lower boundary, the initial value is 
set to $(\mathrm{lower\: boundary +} \mathtt{eps})$. 

Two errors can arise from the root-finding routine. Either 1) it cannot 
find a root in the correct interval or 2) it cannot find a root within 
the required maximum error in \texttt{iter\_max} iteration, where 
\texttt{iter\_max} is set to 30. In case of an error of type 1) 
\texttt{nan} is returned. In the case of type 2) the best estimate of 
the root after \texttt{iter\_max} iterations is returned.

Special care needs to be taken, when degeneracy exits among the $d_k$'s. 
Here we can imagine one of root being pressed in between two infinitely 
close discontinuities. Also, if one $u_k = 0$ there will only be $n-1$ 
roots of the secular equation and the $k$'th eigenvalue will be 
preserved. The following criteria check for these two cases, 
respectively:
\begin{align}
    d_{i+1}-d_{i} < \mathtt{delta} \quad \mathrm{and} \quad  \mathrm{abs}(u_k) < \mathtt{delta}.
\end{align}
In this project \texttt{delta} is chosen to be 1e-7.

Two errors can arise from the root-finding routine. Either 1) it cannot 
find a root in the right interval or 2) it cannot find a root within 
the required maximum error

\texttt{symmetric\_rc\_update} is called in a \texttt{for}-loop, where 
the matrix-size is varied, inside a main function. In this loop a matrix 
of form of eq. (\ref{eq:main}) is constructed. The diagonal elements of 
are arbitrarily chosen to be {-2, -1,...,n} and the elements of 
\textbf{u} are defined according to 
$\mathtt{u_k = ((double)rand()/INT\_MAX-0.5)*2*n}$. The matrix is 
diagonalized using both the \texttt{symmetric\_rc\_update} and the \\ 
\texttt{jacobi\_cyclic} function.

\section{Performance}
 The execution times for both functions was measured and the result is 
plotted as a function of $n$ in fig. \ref{fig:scale}. The execution 
time was measured for $n = 4$, $n = 54$, $n = 104$, $n = 154$ and 
$n = 204$.
 
\begin{figure}[h!]
\input{plot_scale.tex}
\caption{Execution time as a function of the matrix size for both the 
function \texttt{symmetric\_rc\_update} (purple) and 
\texttt{jacobi\_cyclic} (green). To guide the eye an $n^2$ (blue) and 
an $n^3$ (yellow) function are plotted as well.}
\label{fig:scale}
\end{figure}

To verify that the \texttt{symmetric\_rc\_update} finds the correct 
updated eigenvalues the result is compared with the result from the 
\texttt{jacobi\_cyclic} routine for the same $n=4$ matrix used to 
make fig. \ref{fig:secular}. The updated eigenvalues of the matrix

\begin{align}
\mathrm{\mathbf{A}} = 
\begin{bmatrix} 
-2 & 2.72 & 0 & 0 \\
2.72 & -1 & -0.845 & 2.26  \\
0 & -0.845 & 0 & 0 \\
0 & 2.26 & 0  & 1 
\end{bmatrix}
\end{align}
is calculated to be \{-4.74, -0.544, 0.178, 3.11\} in agreement with 
the\\ \texttt{jacobi\_cyclic} routine. This is also presented in the 
file \texttt{out.txt}.

No error occurred for $n = 4$, $n = 54$ and $n = 104$, while for 
$n = 154$ two errors of type 2) occurred and for $n = 204$ four 
errors of type 2). 

\section{Discussion}
Most importantly, the \texttt{symmetric\_rc\_update} uses $O(n^2)$ 
iterations as expected or, in fact, slightly fewer. However, it is 
also clear from  fig. \ref{fig:scale} that it is outperformed by the 
general routine for small matrices, which also performs slightly 
better than expected on this type of matrices. One obvious way to 
improve the speed of the algorithm would be to use a one-dimensional 
root-finding routine. Furthermore, the parameters \texttt{error}, 
\texttt{delta} and \texttt{eps} should be balanced to the needs of 
specific problem. 

It should also be noted that algorithm sometimes makes errors. The 
error of type 1 can be reduced if \texttt{eps} is reduced of the 
cost of more iteration of the root-finding routine. In the actual 
test of the program two errors of type 1) occurred if \texttt{eps} 
was changed from 1e-3 to 5e-3. The errors of type 2) are more 
difficult to avoid. This type of error arise when a root is very 
close to a discontinuity (that is an old eigenvalue), where the 
secular equation is very steep. It should be noted that this 
problem is not restricted by \texttt{iter\_max}, but can be tracked 
down to the precision of data type double. So in fact this not a 
real error in the sense that the root is found within the double 
precision.

\begin{thebibliography}{}
\bibitem{bib:main} Dimitri.V. Fedorov, "Yet Another Introduction 
to Numerical Methods", 2013.
\end{thebibliography}

\end{document}

