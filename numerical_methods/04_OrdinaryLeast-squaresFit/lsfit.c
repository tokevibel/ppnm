#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h> //nescesary
#include"qr_gs.h"
#include<gsl/gsl_blas.h>

void lsfit(int n, double* x, double* y, double* dy,
		double (*funs)(int, double), gsl_vector* c, gsl_matrix* Sigma){

	int m = c->size; //number of parameters

	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_vector* b = gsl_vector_alloc(n);
	for(int i = 0; i<n; i++){
		for(int k =0; k<m; k++){
			gsl_matrix_set(A, i, k, (*funs)(k, x[i])/dy[i]);
		}
		gsl_vector_set(b, i, y[i]/dy[i]);
	}

	gsl_matrix* R = Sigma; //using available matrix of right dimensions
	qr_gs_decomp(A, R); //decomposition of A into QR - Q saved in A
	qr_gs_solve(A, R, b, c);

	gsl_matrix* I = gsl_matrix_alloc(m, m);
	gsl_matrix_set_identity(I);
	gsl_matrix* Rinv = gsl_matrix_alloc(m, m);
	qr_gs_inverse(I, R, Rinv);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Rinv, Rinv, 0.0, Sigma);

	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_matrix_free(I);
	gsl_matrix_free(Rinv);
}

