#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<stdio.h>

void lsfit(int n, double* x, double* y, double* dy, double (*funs)(int, double),
                gsl_vector* c, gsl_matrix* Sigma);

void matrix_print(const char *s, gsl_matrix * A);

double fit(double (*funs)(int, double), gsl_vector* c, double x){
	double f=0;
	for(int i = 0; i<c->size; i++){
		f += (*funs)(i, x)*gsl_vector_get(c, i);
	}
return f;
}

double df(double (*funs)(int, double), gsl_matrix* Sigma, double x){
	int m = Sigma->size1;
	double sum = 0;
	for(int i=0; i<m; i++) for(int j=0; j<m; j++){
		sum += (*funs)(i, x)*gsl_matrix_get(Sigma, i, j)*(*funs)(j, x);
	}
	return sqrt(sum);
}


double funA(int i, double x){
   switch(i){
   case 0: return log(x); break;
   case 1: return 1.0;   break;
   case 2: return x;     break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

int main(int argc, char** argv){
	int n=atoi(argv[1]); //number of data points

	//Load data
	FILE* input = fopen("input.data", "r");
	double x[n], y[n], dy[n];
	for(int i=0; i<n; i++){
		fscanf(input,"%lg %lg %lg", &x[i], &y[i], &dy[i]);
	}
	fclose(input);

	//Fitting
	int m = 3; //number of fitting parameter
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_matrix* Sigma = gsl_matrix_alloc(m, m);
	lsfit(n, x, y, dy, &funA, c, Sigma);
	printf("Fitting data to function f(x) = c_1*log(x)+c_2+c_3*x ...\n");
	printf("Fit result:\n");
	for(int i = 0; i<m; i++){
		printf("c_%d = %g±%g\n", i+1, gsl_vector_get(c, i),
				sqrt(gsl_matrix_get(Sigma, i, i)) );
	}
	matrix_print("Covariance matrix Sigma=",Sigma);

	//Making plot data
	int plotpoints = 200;
	double dt = (x[n-1]-x[0])/(double)(plotpoints-1.0);
	FILE* plot = fopen("plot.data", "w");
	double f, delta_f;
	for(double t = x[0]; t<(x[n-1]+dt); t += dt){
		f = fit(&funA, c, t);
		delta_f = df(&funA, Sigma, t);
		fprintf(plot, "%g %g %g %g\n", t, f, f+delta_f, f-delta_f);
	}
	fclose(plot);

	gsl_vector_free(c);
	gsl_matrix_free(Sigma);
return 0;
}

void matrix_print (const char *s, gsl_matrix * A){
	printf ("%s\n", s);
  	for (int i = 0; i < A->size1; i++){
      		for (int j = 0; j < A->size2; j++){
			printf ("%8.3g ", gsl_matrix_get (A, i, j));
      		}
		printf ("\n");
	}
}
