#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>


//A.2
void f1(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
	double A = 10000.0;
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, A*x0*x1-1);
	gsl_vector_set(fx, 1, exp(-x0)+exp(-x1)-1.0-1.0/A);
	gsl_matrix_set(J, 0, 0, A*x1);
	gsl_matrix_set(J, 0, 1, A*x0);
	gsl_matrix_set(J, 1, 0, -exp(-x0));
	gsl_matrix_set(J, 1, 1, -exp(-x1));
}

// A.3 Rosenbrock's valley function, 
double f2_origin(const gsl_vector* x){
	double a = 1.0;
	double b = 100;
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	double f = (a-x0)*(a-x0)+b*(x1-x0*x0)*(x1-x0*x0);
return f;
}

void f2(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
        double a = 1.0;
	double b = 100;
        double x0 = gsl_vector_get(x, 0);
        double x1 = gsl_vector_get(x, 1);
        gsl_vector_set(fx, 0, -2*(a-x0)-4*b*x0*(x1-x0*x0));
        gsl_vector_set(fx, 1, 2*b*(x1 - x0*x0));
        gsl_matrix_set(J, 0, 0, 2-4*b*x1+12*b*x0*x0);
        gsl_matrix_set(J, 0, 1, -4*b*x0);
        gsl_matrix_set(J, 1, 0, -4*b*x0);
        gsl_matrix_set(J, 1, 1, 2*b);
}

//A.4 Himmelblau
double f3_origin(const gsl_vector* x){
        double x0 = gsl_vector_get(x, 0);
        double x1 = gsl_vector_get(x, 1);
        double f = pow(x0*x0+x1-11, 2) + pow(x0+x1*x1-7, 2);
return f;
}

void f3(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
        double x0 = gsl_vector_get(x, 0);
        double x1 = gsl_vector_get(x, 1);
        gsl_vector_set(fx, 0, 4*x0*(x0*x0+x1-11)+2*(x0+x1*x1-7));
        gsl_vector_set(fx, 1, 2*(x0*x0+x1-11)+4*x1*(x0+x1*x1-7));
        gsl_matrix_set(J, 0, 0, 12*x0*x0+4*x1-44+2);
        gsl_matrix_set(J, 0, 1, 4*x0+4*x1);
        gsl_matrix_set(J, 1, 0, 4*x0+4*x1);
        gsl_matrix_set(J, 1, 1, 2+4*x0+12*x1*x1-28);
}
