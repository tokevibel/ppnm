#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>


void newton_with_jacobian(
	void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
	gsl_vector* x, double epsilon);

void matrix_print (const char *s, gsl_matrix * A);
void vector_print (const char *s, gsl_vector * v);

void f1(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
double f2_origin(const gsl_vector* x);
void f2(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
double f3_origin(const gsl_vector* x);
void f3(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);

int main(){
	//A.2
	printf("\nA.2\n");
	gsl_vector* x1 = gsl_vector_alloc(2);
	gsl_vector_set(x1, 0, 0.5);
	gsl_vector_set(x1, 1, 3);
	vector_print("For initial value of x =", x1);
	double epsilon = 1e-7;
	newton_with_jacobian(f1, x1, epsilon);
	vector_print("Root found is x_root=", x1);
	gsl_vector* fx = gsl_vector_alloc(2);
	gsl_matrix* J = gsl_matrix_alloc(2, 2);
	f1(x1, fx, J);
	vector_print("f(x_root)=", fx);
	printf("\n");

	//A.3
	printf("A.3\n");
	gsl_vector_set(x1, 0, 0);
	gsl_vector_set(x1, 1, 0);
	vector_print("For initial value of x =", x1);
	newton_with_jacobian(f2, x1, epsilon);
	vector_print("Root found is x_root=", x1);
	f2(x1, fx, J);
	vector_print("Gradients(x_root)=", fx);
	printf("f(x_root) = %g\n", f2_origin(x1));
	printf("\n");

	//A.4
	printf("A.4\n");
	gsl_vector_set(x1, 0, 5);
	gsl_vector_set(x1, 1, 5);
	vector_print("For initial value of x =", x1);
	newton_with_jacobian(f3, x1, epsilon);
	vector_print("Root found is x_root=", x1);
	f3(x1, fx, J);
	vector_print("Gradients(x_roots)=", fx);
	printf("f(x_root) = %g\n", f3_origin(x1));

	gsl_vector_free(x1);
	gsl_vector_free(fx);
	gsl_matrix_free(J);
return 0;
}

void vector_print (const char *s, gsl_vector * v){
	printf ("%s", s);
	for (int i = 0; i < v->size; i++)
		printf ("%8.3g ", gsl_vector_get (v, i));
	printf ("\n");
}

void matrix_print (const char *s, gsl_matrix * A){
	printf ("%s\n", s);
	for (int i = 0; i < A->size1; i++){
      		for (int j = 0; j < A->size2; j++){
			printf ("%8.3g ", gsl_matrix_get (A, i, j));
      		}
		printf ("\n");
	}
}
