#include<math.h>
#include<stdio.h>

double adapt(double f(double), double a, double b,
        double acc, double eps, double *err);


int main(){
	printf("\nExercise Adaptive Integration\n\n");
	printf("A. Recursive adaptive integrator\n");

	double a=0, b=1, err, acc=0.0001, eps=0.0001;

	printf("In following three integrals acc = 1e-4, eps =1e-4.\n");
	printf("\nIntegral of sqrt(x) from x=0 to x=1:\n");
 	int calls = 0;
	double my_func1(double x){calls++; return sqrt(x);}
	double Q1 = adapt(my_func1, a, b, acc, eps, &err);
	printf("integral=%.8lg, estimated error=%.2lg, function calls = %d\n",
		Q1, err, calls);
	printf("Exact result = 6.66666666...\n");

	printf("\nIntegral of 1/sqrt(x) from x=0 to x=1:\n");
	calls = 0;
	double my_func2(double x){calls++; return 1./sqrt(x);}
	double Q2 = adapt(my_func2, a, b, acc, eps, &err);
	printf("integral=%.8lg, estimated error=%.2lg, function calls = %d\n",
		Q2, err, calls);
	printf("Exact result = 2\n");

	printf("\nIntegral of ln(x)/sqrt(x) from x=0 to x=1:\n");
	calls = 0;
	double my_func3(double x){calls++; return log(x)/sqrt(x);}
	double Q3 = adapt(my_func3, a, b, acc, eps, &err);
	printf("integral=%.8lg, estimated error=%.2lg, function calls = %d\n",
		Q3, err, calls);
	printf("Exact result = -4\n");

	printf("\nIntegral of ln(x)/sqrt(x) from x=0 to x=1:\n");
	printf("Using acc=1e-11, eps=1e-11 the result stagnates in double precision.\n");
	a=0; b=1; acc=1e-11; eps=1e-11;
	calls = 0;
	double my_func4(double x){calls++; return 4*sqrt(1-(1-x)*(1-x));}
	double Q4 = adapt(my_func4, a, b, acc, eps, &err);
	printf("integral=%.20lg, estimated error=%.2lg, function calls = %d\n",
		Q4, err, calls);
	printf("pi_exact=3.141592653589793238\n");

	return 0;
}

