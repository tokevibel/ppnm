#include<math.h>
#include<assert.h>

double adapt24(double f(double), double a, double b, double acc, double eps,
	double f2, double f3, double *err, int 	nrec){
	assert(nrec<1000000);
	double f1 = f(a+(b-a)/6);
	double f4 = f(a+5*(b-a)/6);

	double Q = (2*f1+f2+f3+2*f4)/6*(b-a);
	double q = (f1+f2+f3+f4)/4*(b-a);
	double tolerance = acc+eps*fabs(Q);
	*err = fabs(Q-q);

	if(*err<tolerance) return Q;
	else{
		double Q1 = adapt24(f, a, (a+b)/2, acc/sqrt(2.), eps, f1, f2,
			err, nrec+1);
		double Q2 = adapt24(f, (a+b)/2, b, acc/sqrt(2.), eps, f3, f4,
			err, nrec+1);
		return Q1+Q2;
	}
}

double adapt(double f(double), double a, double b,
	double acc, double eps, double *err){

	double f2 = f(a+2*(b-a)/6);
	double f3 = f(a+4*(b-a)/6);
	int nrec = 0;

	return adapt24(f, a, b, acc, eps, f2, f3, err, nrec);
}



