#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_blas.h>
#include<time.h>

void matrix_print(const char *s, gsl_matrix * A);
void vector_print(const char *s, gsl_vector * v);
int  jacobi_cyclic(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

int main(){
	//Generate random symmetric matrix
	printf("\nA\n\n");
	int n = 4;
	gsl_matrix* A = gsl_matrix_alloc(n, n);
	for(int i = 0; i < n; i++){
		gsl_matrix_set(A, i, i, (double)rand()/(double)RAND_MAX);
		for(int j = i+1; j<n; j++){
		double x = (double)rand()/(double)RAND_MAX;
		gsl_matrix_set(A, i, j, x);
		gsl_matrix_set(A, j, i, x);
		}
	}
	matrix_print("The random symmetric matrix, A=", A);
	gsl_vector* e = gsl_vector_alloc(n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	jacobi_cyclic(A, e, V);
	//reconstruct A
	for(int i = 0; i<n; i++)for(int j=i+1; j<n; j++){
		gsl_matrix_set(A, i, j, gsl_matrix_get(A, j, i));
	}
	gsl_matrix* TEMP = gsl_matrix_calloc(n, n);
	for(int i = 0; i<n; i++) gsl_matrix_set(TEMP, i, i, gsl_vector_get(e,i));
	matrix_print("can be eigenvaluedecomposed in to VDVt with V=", V);
	matrix_print("and D=", TEMP);

	//Checking
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, V, 0.0, TEMP);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, V, TEMP, 0.0, A);
	matrix_print("Checking that D=VtAV=",A);
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_matrix_free(TEMP);
	gsl_vector_free(e);

	//Measure diagonalization time as function of n
	FILE* data = fopen("scaling.data", "w");
	clock_t t;
	for(int n = 2; n<203; n=n+50){
		gsl_matrix* A = gsl_matrix_alloc(n, n);
			for(int i = 0; i < n; i++){
				gsl_matrix_set(A, i, i, (double)rand()/(double)RAND_MAX);
				for(int j = i+1; j<n; j++){
					double x = (double)rand()/(double)RAND_MAX;
					gsl_matrix_set(A, i, j, x);
					gsl_matrix_set(A, j, i, x);
				}
			}
		gsl_vector* e = gsl_vector_alloc(n);
		gsl_matrix* V = gsl_matrix_alloc(n,n);
		t = clock();
		jacobi_cyclic(A, e, V);
		t = clock()-t;
		gsl_vector_free(e);
		gsl_matrix_free(A);
		gsl_matrix_free(V);
		fprintf(data, "%d %g %g\n", n, (double)t/CLOCKS_PER_SEC, 10e-7*n*n*n);
	}
	fclose(data);
return 0;
}

void vector_print (const char *s, gsl_vector * v){
	printf ("%s", s);
	for (int i = 0; i < v->size; i++)
	printf ("%8.3g ", gsl_vector_get (v, i));
	printf ("\n");
}

void matrix_print (const char *s, gsl_matrix * A){
	printf ("%s\n", s);
	for (int i = 0; i < A->size1; i++){
		for (int j = 0; j < A->size2; j++){
			printf ("%8.3g ", gsl_matrix_get (A, i, j));
		}
		printf ("\n");
	}
}
