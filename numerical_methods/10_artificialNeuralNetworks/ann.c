#include<gsl/gsl_vector.h>
#include"ann.h"
#include<stdio.h>
#include<gsl/gsl_multimin.h>

//typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;

ann* ann_alloc(int number_of_hidden_neurons, double (*activation_function)(double)){
	ann* network = malloc(sizeof(ann));
	network->n = number_of_hidden_neurons;
	network->f = activation_function;
	network->data = gsl_vector_alloc(3*number_of_hidden_neurons);
	return network;
}

void ann_free(ann* network){
	gsl_vector_free(network->data);
	free(network);
}

double ann_feed_forward(ann* network, double x){
	double sum = 0;
	int n = network->n;
		double ai, bi, wi;
	for(int i = 0; i<n; i++){
		ai = gsl_vector_get(network->data, i*3);
		bi = gsl_vector_get(network->data, i*3+1);
		wi = gsl_vector_get(network->data, i*3+2);
		sum += network->f((x-ai)/bi)*wi;
	}
	return sum;
}

void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist){
	int n = network->n;
	int N = xlist->size;

	double delta_p(const gsl_vector* x, void* params){ 
	//void* params is included to satisfy the GSL library
		double sum = 0, x_k, y_k;
		for(int i=0;i<3*n;i++){
			gsl_vector_set(network->data, i, gsl_vector_get(x, i));
		}
		for(int k = 0; k<N; k++){
			x_k = gsl_vector_get(xlist, k);
			y_k = gsl_vector_get(ylist, k);
			sum += pow(ann_feed_forward(network, x_k)-y_k, 2);
		}
		return sum;
	}

	int dim = 3*n;
	gsl_multimin_function F;
	F.f = delta_p;
	F.n = dim;
	F.params = NULL;

	gsl_multimin_fminimizer *M;
	#define TYPE gsl_multimin_fminimizer_nmsimplex2
	M = gsl_multimin_fminimizer_alloc(TYPE, dim);
	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector* step = gsl_vector_alloc(dim);

	gsl_vector_set(start,0,1);
	gsl_vector_set(start,1,1);
	gsl_vector_set(start,2,-1);
	gsl_vector_set(start,3,1);
	gsl_vector_set(start,4,0.5);
	gsl_vector_set(start,5,1);
	gsl_vector_set(start,6,-6);
	gsl_vector_set(start,7,1);
	gsl_vector_set(start,8,1);

	gsl_vector_set(step,0,0.01);
	gsl_vector_set(step,1,0.01);
	gsl_vector_set(step,2,0.01);
	gsl_vector_set(step,3,0.01);
	gsl_vector_set(step,4,0.01);
	gsl_vector_set(step,5,0.01);
	gsl_vector_set(step,6,0.01);
	gsl_vector_set(step,7,0.01);
	gsl_vector_set(step,8,0.01);

	gsl_multimin_fminimizer_set(M, &F, start, step);

	int iter=0,status;
	double size;
	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(M);
		if (status) break;

		size = gsl_multimin_fminimizer_size (M);
		status = gsl_multimin_test_size (size, 1e-6);

		if (status == GSL_SUCCESS){
	          	printf ("converged to minimum at\n");
        		}

		//printf("%d\n", iter);
	}while (status == GSL_CONTINUE && iter < 100000);

	gsl_multimin_fminimizer_free(M);
	gsl_vector_free(start);
	gsl_vector_free(step);
}
