#include<gsl/gsl_vector.h>
#include<stdio.h>
#include"ann.h"
#include<math.h>

double gauss(double x){
	return cos(5*x)*exp(-x*x);
}

int main(){
	int n = 100; //number of training points
	gsl_vector* xlist = gsl_vector_alloc(n);
	gsl_vector* ylist = gsl_vector_alloc(n);
	FILE* train = fopen("train.data","w");
	for(int i = 0; i<n; i++){
		double x = i*2*M_PI/n;
		gsl_vector_set(xlist, i, x);
		gsl_vector_set(ylist, i, sin(x));
		fprintf(train, "%g %g\n", x, sin(x));
	}
	fclose(train);
	ann* network = ann_alloc(3, &gauss);
	ann_train(network, xlist, ylist);
	FILE* predict = fopen("predict.data","w");
	double y_predict;
	for(double x = -6.3 ; x<12.6; x+=0.1){
		y_predict =  ann_feed_forward(network, x);
		fprintf(predict, "%g %g %g\n", x, y_predict, sin(x));
	}
	fclose(predict);
	ann_free(network);
	FILE* out = fopen("out.txt", "w");
	fprintf(out, "\nExercise Artificial Neural Networks\n\n");
	fprintf(out, "A.\n");
	fprintf(out, "For %d tabulated values of the sine function\n", n);
	fprintf(out, "and a wavelet, f(x)=cos(5x)×exp(-x²), as\n");
	fprintf(out, "activation function, the predicted function\n");
	fprintf(out, "can be seen in \"plot.svg\"\n\n");
	fclose(out);
return 0;
}
