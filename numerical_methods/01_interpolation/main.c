#include<math.h>
#include<stdio.h>
#include"qspline.h"

double linterp(int n, double* x, double* y, double z);
double linterp_integ(int n, double *x, double *y, double z);
void test_quad_like_hint_suggests(void);

int main(){
	int n = 10;
	double x[n];
	double y[n];
	double dx = 2*M_PI/(n-1);

	FILE* points = fopen("points.data", "w");
	for(int i = 0;i<n;i++){
		x[i] = i*dx;
		y[i] = sin(i*dx);
		fprintf(points,"%g %g\n", x[i], y[i]);
	}
	fclose(points);

	/* Linear interpolation */
	FILE* data = fopen("analysis.data", "w");
	for(double z = 0; z<= 2*M_PI; z+=0.05){
		fprintf(data, "%g %g %g %g %g\n", z, linterp(n,x,y,z), sin(z),
		linterp_integ(n,x,y,z),1-cos(z));
	}

	/* quadratic spline */
	qspline *s = qspline_alloc(n,x,y);
	fprintf(data,"\n\n");
	for(double z = 0; z<= 2*M_PI; z+=0.05){
		fprintf(data, "%g %g %g %g %g %g %g\n", z, qspline_eval(s,z), sin(z),
		qspline_derivative(s,z), cos(z), qspline_integral(s,z),1-cos(z));
        }
	qspline_free(s);

	fclose(data);

	test_quad_like_hint_suggests();

return 0;
}
