#ifndef HAVE_QSPLINE_H /* for multiple includes */

typedef struct {int n; double *x, *y, *b, *c;} qspline;

qspline* qspline_alloc(int n, double *x, double *y); /* allocates and builds the quadratic spline */
double qspline_eval(qspline *s, double z);        /* evaluates the prebuilt spline at point z */
double qspline_derivative(qspline *s, double z); /* evaluates the derivative of the prebuilt spline at point z */
double qspline_integral(qspline *s, double z);  /* evaluates the integral of the prebuilt spline from x[0] to z */
void qspline_free(qspline *s); /* free memory allocated in qspline_alloc */

#define HAVE_QSPLINE_H
#endif
