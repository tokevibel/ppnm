#include<stdio.h>

int binary_search(int n, double *x, double z);

double linterp_integ(int n, double *x, double *y, double z){
	int j = binary_search(n,x,z);
	double integ = 0;
	for(int i = 0; i < j; i++){
		integ += 0.5*(y[i]+y[i+1])*(x[i+1]-x[i]);
		//printf("Error\n");
	}
	// calculating the y(z)
	double p = (y[j+1]-y[j]) / (x[j+1]-x[j]);
        double y_z  = y[j] + p*(z-x[j]);
	integ += 0.5*(y[j]+y_z)*(z-x[j]);
return integ;
}
