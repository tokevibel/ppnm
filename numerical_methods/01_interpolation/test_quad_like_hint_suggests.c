#include<stdio.h>
#include"qspline.h"

void test_quad_like_hint_suggests(void){
	//test parameters for quadratic spline
	int nt = 5; //number data point in test
	double x_test[nt];
	double y0[nt];
	double y1[nt];
	double y2[nt];
	for(int i = 1; i<=nt; i++){
		x_test[i-1] = i;
		y0[i-1] =1;
		y1[i-1] =i;
		y2[i-1] =i*i;
	}
	//test 1
	qspline *s0 = qspline_alloc(nt,x_test,y0);
	printf("\nFor {xi=i, yi=1} , i=1,...,5 expectations are b=0, c=0\n");
	printf("The quadratic spline gives b = ");
	for(int i = 0; i<nt-1; i++){
		printf("%g, ",s0->b[i]);
	}
	printf("\nAnd c = ");
        for(int i = 0; i<nt-1; i++){
                printf("%g, ",s0->c[i]);
        }
	printf("\n");
	qspline_free(s0);
	//test 2
	qspline *s1 = qspline_alloc(nt,x_test,y1);
        printf("\nFor {xi=i, yi=i} , i=1,...,5 expectations are b=1, c=0\n");
        printf("The quadratic spline gives b = ");
        for(int i = 0; i<nt-1; i++){
                printf("%g, ",s1->b[i]);
        }
        printf("\nAnd c = ");
        for(int i = 0; i<nt-1; i++){
                printf("%g, ",s1->c[i]);
        }
        printf("\n");
        qspline_free(s1);

	//test 3
	qspline *s2 = qspline_alloc(nt,x_test,y2);
        printf("\nFor {xi=i, yi=i²} , i=1,...,5 expectations are b={2,4,6,8}, c=1\n");
        printf("The quadratic spline gives b = ");
        for(int i = 0; i<nt-1; i++){
                printf("%g, ",s2->b[i]);
        }
        printf("\nAnd c = ");
        for(int i = 0; i<nt-1; i++){
                printf("%g, ",s2->c[i]);
        }
        printf("\n");
        qspline_free(s2);
}
