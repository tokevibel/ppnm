#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void newton(
	double f(const gsl_vector* x, gsl_vector* df, gsl_matrix* H), /* f: objective function, df: gradient, H: Hessian matrix H*/
	gsl_vector* xstart, /* starting point, becomes the latest approximation to the root on exit */
	double eps, /* accuracy goal, on exit |gradient|<eps */
	int* steps);

void matrix_print (const char *s, gsl_matrix * A);
void vector_print (const char *s, gsl_vector * v);

double f_ros(const gsl_vector* x, gsl_vector* df, gsl_matrix* H);
double f_himmel(const gsl_vector* x, gsl_vector* df, gsl_matrix* H);

int main(){
	//A.2
	printf("\nA. Rosenbrock's valley function\n");
	gsl_vector* x1 = gsl_vector_alloc(2);
	gsl_vector_set(x1, 0, 0);
	gsl_vector_set(x1, 1, 0);
	vector_print("For initial value of x =", x1);
	double epsilon = 1e-7;
	printf("and epsilon = %g\n",epsilon);
	int steps;
	newton(f_ros, x1, epsilon, &steps);
	vector_print("Root found is x_root=", x1);
	gsl_vector* df = gsl_vector_alloc(2);
	gsl_matrix* H = gsl_matrix_alloc(2, 2);
	double fx = f_ros(x1, df, H);
	vector_print("Gradients(x_root)=", df);
	printf("f(x_root) = %g\n", fx);
	printf("Number of steps = %d\n", steps);

	//A.4
	printf("\nA. Himmelblau's function\n");
	gsl_vector_set(x1, 0, 5);
	gsl_vector_set(x1, 1, 5);
	vector_print("For initial value of x =", x1);
	printf("and epsilon = %g\n",epsilon);
	newton(f_himmel, x1, epsilon, &steps);
	vector_print("Root found is x_root=", x1);
	fx = f_himmel(x1, df, H);
	vector_print("Gradients(x_roots)=", df);
	printf("f(x_root) = %g\n", fx);
	printf("Number of steps = %d\n", steps);

	gsl_vector_free(x1);
	gsl_vector_free(df);
	gsl_matrix_free(H);
return 0;
}

void vector_print (const char *s, gsl_vector * v){
	printf ("%s", s);
	for (int i = 0; i < v->size; i++)
		printf ("%8.3g ", gsl_vector_get (v, i));
	printf ("\n");
}

void matrix_print (const char *s, gsl_matrix * A){
	printf ("%s\n", s);
	for (int i = 0; i < A->size1; i++){
      		for (int j = 0; j < A->size2; j++){
			printf ("%8.3g ", gsl_matrix_get (A, i, j));
      		}
		printf ("\n");
	}
}
