#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>

void matrix_print (const char *s, gsl_matrix * A);
void vector_print (const char *s, gsl_vector * v);

double f_ros(const gsl_vector* x, gsl_vector* df, gsl_matrix* H){
	double a = 1.0;
	double b = 100;
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	double f = (a-x0)*(a-x0)+b*(x1-x0*x0)*(x1-x0*x0);

	gsl_vector_set(df, 0, -2*(a-x0)-4*b*x0*(x1-x0*x0));
	gsl_vector_set(df, 1, 2*b*(x1 - x0*x0));

	gsl_matrix_set(H, 0, 0, 2-4*b*x1+12*b*x0*x0);
	gsl_matrix_set(H, 0, 1, -4*b*x0);
	gsl_matrix_set(H, 1, 0, -4*b*x0);
	gsl_matrix_set(H, 1, 1, 2*b);
return f;
}
double f_himmel(const gsl_vector* x, gsl_vector* df, gsl_matrix* H){
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	double f = pow(x0*x0+x1-11, 2) + pow(x0+x1*x1-7, 2);

	gsl_vector_set(df, 0, 4*x0*(x0*x0+x1-11)+2*(x0+x1*x1-7));
	gsl_vector_set(df, 1, 2*(x0*x0+x1-11)+4*x1*(x0+x1*x1-7));

	gsl_matrix_set(H, 0, 0, 12*x0*x0+4*x1-44+2);
	gsl_matrix_set(H, 0, 1, 4*x0+4*x1);
	gsl_matrix_set(H, 1, 0, 4*x0+4*x1);
	gsl_matrix_set(H, 1, 1, 2+4*x0+12*x1*x1-28);
return f;
}
