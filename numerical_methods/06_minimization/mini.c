#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"

void newton(double f(const gsl_vector* x, gsl_vector* df, gsl_matrix* H),
		gsl_vector* xstart, double eps, int* steps){

	gsl_vector* x = xstart;
	gsl_vector* df = gsl_vector_alloc(x->size);
	gsl_matrix* H = gsl_matrix_alloc(x->size, x->size);
	gsl_matrix* R = gsl_matrix_alloc(H->size1, H->size2);
	gsl_vector* dx = gsl_vector_alloc(x->size);

	f(x, df, H);
	double lambda;
	double lambda_min = 1.0/65.0;
	gsl_vector* df_temp = gsl_vector_alloc(df->size);
	gsl_vector* x_temp = gsl_vector_alloc(x->size);
	double alpha = 1e-4;
	double dot_p;
	double phi, phi_temp;
	*steps = 0;
	while(gsl_blas_dnrm2(df)>eps){
		phi = f(x, df, H);
		qr_gs_decomp(H, R);

		gsl_vector_scale(df, -1.0);
		qr_gs_solve(H, R, df, dx);

		lambda = 1;
		gsl_vector_memcpy(x_temp, x);
		gsl_blas_daxpy(lambda, dx, x_temp);
		phi_temp = f(x_temp, df_temp, H);

		gsl_blas_ddot(dx, df, &dot_p);
		while(phi_temp>=phi+alpha* dot_p && lambda>lambda_min){
			lambda /=2;
			gsl_vector_scale(dx, 0.5); //halve step size
			gsl_blas_daxpy(-1.0, dx, x_temp);
			phi_temp=f(x_temp, df_temp, H);
			gsl_blas_ddot(dx, df, &dot_p);
		}
		gsl_vector_memcpy(x, x_temp);
		gsl_vector_memcpy(df, df_temp);
		(*steps)++;
	}

	gsl_vector_free(df);
	gsl_matrix_free(H);
	gsl_matrix_free(R);
	gsl_vector_free(dx);
	gsl_vector_free(df_temp);
	gsl_vector_free(x_temp);
}


