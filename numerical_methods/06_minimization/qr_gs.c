#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
	int n = A->size1;
	int m = A->size2;
	double Rii, Rij;
	for(int i=0; i<m; i++){
		Rii=0;
		// Rii = sqrt(ai^T(dot)ai)
		for(int j=0; j<n; j++){
			Rii += pow(gsl_matrix_get(A, j, i), 2);
		}
		Rii = sqrt(Rii);
		gsl_matrix_set(R, i, i, Rii);
		// qi = ai/Rii
		for(int j=0; j<n;j++){
			gsl_matrix_set(A, j, i, gsl_matrix_get(A, j, i)/Rii);
		}
		//Rij = qi^T(dot)aj
		for(int j = i+1; j<m; j++){
			Rij = 0;
			for(int k=0; k<n; k++){
			Rij += gsl_matrix_get(A, k, i)*gsl_matrix_get(A, k, j);
			}
			gsl_matrix_set(R, i, j, Rij);
			// aj = aj - Rij * ai
			for(int k =0; k<n; k++){
				gsl_matrix_set(A, k, j, gsl_matrix_get(A, k, j)-Rij*gsl_matrix_get(A,k,i));;
			}
		}
	}
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x){
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	for(int i=x->size-1; i>=0; i--){
		double s=gsl_vector_get(x, i);
		for(int k=i+1; k<x->size; k++){
			s-=gsl_matrix_get(R, i, k)*gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, s/gsl_matrix_get(R, i, i));
	}
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
	gsl_matrix_set_identity(B);
	gsl_vector* x = gsl_vector_alloc(Q->size1);
	for(int i = 0; i<Q->size2; i++){
		gsl_vector_view v = gsl_matrix_column(B,i);
		qr_gs_solve(Q, R, &v.vector, x);
		gsl_matrix_set_col(B, i, x);
	}
	gsl_vector_free(x);
}
