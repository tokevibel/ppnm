#include<math.h>
#include<stdio.h>


void plainmc(int dim, double *a, double *b,
	double f(double* x), int N, double* result, double* error);

double sinus_box(double* x){
	return sin(M_PI*x[0])*sin(2*M_PI*x[1])*sin(3*M_PI*x[2]);
}

double gauss_sphere(double* x){
	double sigma = 0.5;
	return exp(-(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])/(2*sigma*sigma));
}

double test_func(double* x){
	return (pow(M_PI,-3)/(1-cos(x[0])*cos(x[1])*cos(x[2])));
}

int main(){
	printf("\nExercise Monte Carlo integration\n\n");
	printf("A. Plain Monte Carlo integration\n");
	printf("Number of point, N = 1e6\n\n");
	int dim = 3;
	double a[dim], b[dim];
	for(int i = 0; i<dim; i++){a[i] = 0; b[i] = 1;}
	int N = 1e6;
	double result, error;
	plainmc(dim, a, b, sinus_box, N, &result, &error);
	printf("Integral of sin(pi*x)*sin(2*pi*y)*sin(3*pi*z)\n");
	printf("in interval x=0..1, y=0..1, z=0..1\n");
	printf("Calculated integral = %g\nError = %g\n", result, error);
	printf("Theoretical value = 0.0\n\n");

	for(int i = 0; i<dim; i++){a[i] = 0; b[i] = 5;}
	plainmc(dim, a, b, gauss_sphere, N, &result, &error);
	printf("Integral of exp(-(x²+y²+z²)/(2*sigma²))\n");
	printf("with sigma = 0.5");
	printf("in interval x=0..5, y=0..5, z=0..5\n");
	printf("Calculated value = %g\nError = %g\n", result, error);
	printf("Theoretical value = (1/(2*sqrt(2))*pi^(3/2)*sigma³≈0.246087\n\n");

	for(int i = 0; i<dim; i++){a[i] = 0; b[i] = M_PI;}
	plainmc(dim, a, b, test_func, N, &result, &error);
	printf("Integral of (1/pi³)*(1-cos(x[0])*cos(x[1])*cos(x[2]))^-1\n");
	printf("in interval x=0..pi, y=0..pi, z=0..pi\n");
	printf("Calculated value = %g\nError = %g\n", result, error);
	printf("Theoretical value Γ(1/4)^4/(4π^3) ≈ 1.39320393\n\n");

	//Exercise B;
	FILE* error_data = fopen("error.data","w");
	for(int i = 1; i<9; i++){
		N = (double)pow(10,i);
		plainmc(dim, a, b, test_func, N, &result, &error);
		fprintf(error_data, "%d %g %g %g\n", N, error,
		fabs(result-1.393203929685677), 1/sqrt(N));
	}
	fclose(error_data);
return 0;
}
