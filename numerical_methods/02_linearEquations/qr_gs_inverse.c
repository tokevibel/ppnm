#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x);

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
	gsl_matrix_set_identity(B);
	gsl_vector* x = gsl_vector_alloc(Q->size1);
	for(int i = 0; i<Q->size2; i++){
		gsl_vector_view v = gsl_matrix_column(B,i);
		qr_gs_solve(Q, R, &v.vector, x);
		gsl_matrix_set_col(B, i, x);
	}
	gsl_vector_free(x);
}
