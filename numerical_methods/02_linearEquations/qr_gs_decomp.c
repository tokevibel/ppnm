#include<gsl/gsl_matrix.h>
#include<math.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
	int n = A->size1;
	int m = A->size2;
	double Rii, Rij;
	for(int i=0; i<m; i++){
		Rii=0;
		// Rii = sqrt(ai^T(dot)ai)
		for(int j=0; j<n; j++){
			Rii += pow(gsl_matrix_get(A, j, i), 2);
		}
		Rii = sqrt(Rii);
		gsl_matrix_set(R, i, i, Rii);
		// qi = ai/Rii
		for(int j=0; j<n;j++){
			gsl_matrix_set(A, j, i, gsl_matrix_get(A, j, i)/Rii);
		}
		//Rij = qi^T(dot)aj
		for(int j = i+1; j<m; j++){
			Rij = 0;
			for(int k=0; k<n; k++){
			Rij += gsl_matrix_get(A, k, i)*gsl_matrix_get(A, k, j);
			}
			gsl_matrix_set(R, i, j, Rij);
			// aj = aj - Rij * ai
			for(int k =0; k<n; k++){
				gsl_matrix_set(A, k, j, gsl_matrix_get(A, k, j)-Rij*gsl_matrix_get(A,k,i));;
			}
		}
	}
}
