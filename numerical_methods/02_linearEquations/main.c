#include<gsl/gsl_matrix.h>
#include<stdlib.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R,const gsl_vector* b,gsl_vector* x);
void matrix_print (const char *, gsl_matrix * m);
void vector_print (const char *s, gsl_vector * v);
void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B);

int main(){
	// A.1
	size_t n1 =4;
	size_t n2 =3;
	gsl_matrix* A = gsl_matrix_alloc(n1, n2);
	gsl_matrix* R = gsl_matrix_alloc(n2, n2);
	for(size_t i = 0; i<n1; i++){
		for(size_t j = 0; j<n2; j++){
			gsl_matrix_set(A,i,j,(double)rand()/(double)RAND_MAX);
		}
	}
	printf("\nExercise A.1\n\n");
        matrix_print("A=",A);
	gsl_matrix* Q = gsl_matrix_alloc(n1,n2);
	gsl_matrix_memcpy(Q, A);
	qr_gs_decomp(Q, R);
	matrix_print("Q=",Q);
	matrix_print("R=",R);
	gsl_matrix* QR = gsl_matrix_calloc(n1, n2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Q, R, 0.0, QR);
	matrix_print("QR =",QR);
	gsl_matrix* QtQ = gsl_matrix_calloc(n2, n2);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, Q, Q, 0.0, QtQ);
	matrix_print("Q^{T}Q=",QtQ);

	gsl_matrix_free(A);
	gsl_matrix_free(QtQ);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_matrix_free(QR);

	//A.2
	int n =4;
	gsl_matrix* A2 = gsl_matrix_alloc(n, n);
	gsl_vector* b =  gsl_vector_alloc(n);
	gsl_vector* x =  gsl_vector_calloc(n);
	for(size_t i = 0; i<n; i++){
		for(size_t j = 0; j<n; j++){
			gsl_matrix_set(A2,i,j,(double)rand()/(double)RAND_MAX);
		}
		gsl_vector_set(b, i, (double)rand()/(double)RAND_MAX);
	}
	printf("\nExercise A.2\n\n");
	matrix_print("A=",A2);
	vector_print("b=",b);
	gsl_matrix* R2 = gsl_matrix_alloc(n, n);
	gsl_matrix* Q2 = gsl_matrix_alloc(n, n);
	gsl_matrix_memcpy(Q2, A2);
	qr_gs_decomp(Q2, R2);
	qr_gs_solve(Q2, R2, b, x);
	printf("Find x, such that Ax=b\n");
	vector_print("x=",x);
	gsl_vector* bt = gsl_vector_calloc(n);
	gsl_blas_dgemv(CblasNoTrans, 1.0, A2, x, 0.0, bt);
	vector_print("Ax =",bt);

	gsl_matrix_free(A2);
	gsl_matrix_free(R2);
	gsl_matrix_free(Q2);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(bt);

	//B
	int n3 =4;
	gsl_matrix* A3 = gsl_matrix_alloc(n3, n3);
	for(size_t i = 0; i<n3; i++){
		for(size_t j = 0; j<n3; j++){
			gsl_matrix_set(A3,i,j,(double)rand()/(double)RAND_MAX);
		}
		gsl_vector_set(b, i, (double)rand()/(double)RAND_MAX);
	}
	printf("\nExercise B\n\n");
	gsl_matrix* Q3 = gsl_matrix_alloc(n3,n3);
	gsl_matrix_memcpy(Q3, A3);
	gsl_matrix* R3 = gsl_matrix_alloc(n3, n3);
	qr_gs_decomp(Q3,R3);
	gsl_matrix* B = gsl_matrix_alloc(n3,n3);
	qr_gs_inverse(Q3, R3, B);
	matrix_print("A=",A3);
	matrix_print("Inverse of A, B=", B);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A3, B, 0.0, R3);
	matrix_print("AB=",R3);

	gsl_matrix_free(A3);
	gsl_matrix_free(Q3);
	gsl_matrix_free(R3);
	gsl_matrix_free(B);
return 0;
}

void vector_print (const char *s, gsl_vector * v){
	printf ("%s", s);
	for (int i = 0; i < v->size; i++)
	printf ("%8.3g ", gsl_vector_get (v, i));
	printf ("\n");
}

void matrix_print (const char *s, gsl_matrix * A){
	printf ("%s\n", s);
  	for (int i = 0; i < A->size1; i++){
      		for (int j = 0; j < A->size2; j++){
			printf ("%8.3g ", gsl_matrix_get (A, i, j));
      		}
		printf ("\n");
	}
}
