#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x){
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	for(int i=x->size-1; i>=0; i--){
		double s=gsl_vector_get(x, i);
		for(int k=i+1; k<x->size; k++){
			s-=gsl_matrix_get(R, i, k)*gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, s/gsl_matrix_get(R, i, i));
	}
}

