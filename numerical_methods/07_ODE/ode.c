#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"ode.h"

void rkstep45(
	double t,					/* the current value of the variable */
	double h,					/* the step to be taken */
	gsl_vector* yt,					/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt), /* the right-hand-side, dydt = f(t,y) */
	gsl_vector* yth,				/* output: y(t+h) */
	gsl_vector* err					/* output: error estimate dy */
	){

	int n = yt->size;
	gsl_vector* k1 = gsl_vector_alloc(n);
	gsl_vector* k2 = gsl_vector_alloc(n);
	gsl_vector* k3 = gsl_vector_alloc(n);
	gsl_vector* k4 = gsl_vector_alloc(n);
	gsl_vector* k5 = gsl_vector_alloc(n);
	gsl_vector* k6 = gsl_vector_alloc(n);

	gsl_vector* yttemp = gsl_vector_alloc(n);

	f(t, yt, k1);

	gsl_blas_dcopy(yt, yttemp);
	gsl_blas_daxpy(1.0/4*h, k1, yttemp);
	f(t+1.0/4*h, yttemp, k2);

	gsl_blas_dcopy(yt, yttemp);
	gsl_blas_daxpy(3.0/32*h, k1, yttemp);
	gsl_blas_daxpy(9.0/32*h, k2, yttemp);
	f(t+3.0/8*h, yttemp, k3);

	gsl_blas_dcopy(yt, yttemp);
	gsl_blas_daxpy(1932.0/2197*h, k1, yttemp);
	gsl_blas_daxpy(-7200.0/2197*h, k2, yttemp);
	gsl_blas_daxpy(7296.0/2197*h, k3, yttemp);
	f(t+12.0/13*h, yttemp, k4);

	gsl_blas_dcopy(yt, yttemp);
	gsl_blas_daxpy(439.0/216*h, k1, yttemp);
	gsl_blas_daxpy(-8.0*h, k2, yttemp);
	gsl_blas_daxpy(3680.0/513*h, k3, yttemp);
	gsl_blas_daxpy(-845.0/4104*h, k4, yttemp);
	f(t+h, yttemp, k5);

	gsl_blas_dcopy(yt, yttemp);
	gsl_blas_daxpy(-8.0/27*h, k1, yttemp);
	gsl_blas_daxpy(2.0*h, k2, yttemp);
	gsl_blas_daxpy(-3544.0/2565*h, k3, yttemp);
	gsl_blas_daxpy(1859.0/4104*h, k4, yttemp);
	gsl_blas_daxpy(-11.0/40*h, k5, yttemp);
	f(t+1.0/2*h, yttemp, k6);

	gsl_blas_dcopy(yt, yth);
	gsl_blas_daxpy(16.0/135*h, k1, yth);
	gsl_blas_daxpy(6656.0/12825*h, k3, yth);
	gsl_blas_daxpy(28561.0/56430*h, k4, yth);
	gsl_blas_daxpy(-9.0/50*h, k5, yth);
	gsl_blas_daxpy(2.0/55*h, k6, yth);

	gsl_blas_dcopy(yt, yttemp);
	gsl_blas_daxpy(25.0/216*h, k1, yttemp);
	gsl_blas_daxpy(1408.0/2565*h, k3, yttemp);
	gsl_blas_daxpy(2197.0/4104*h, k4, yttemp);
	gsl_blas_daxpy(-1.0/5*h, k5, yttemp);

	gsl_blas_dcopy(yth, err);
	gsl_blas_daxpy(-1.0, yttemp, err);

	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(k4);
	gsl_vector_free(k5);
	gsl_vector_free(k6);
	gsl_vector_free(yttemp);
}


void driver(
	double* t,				/* the current value of the variable */
	double b,				/* the end-point of the integration */
	double* h,				/* the current step-size */
	gsl_vector* yt,				/* the current y(t) */
	double acc,				/* absolute accuracy goal */
	double eps,				/* relative accuracy goal */
	void stepper(				/* the stepper function to be used */
		double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void f(double t, gsl_vector* y, gsl_vector* dydt) /* right-hand-side */
	){
	double a = *t;
	//int k = 0;
	int n = yt->size;
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	double err_norm, tolerance;
	while(*t<b){
		if(*t+*h>b) *h=b-*t;
		stepper(*t, *h, yt, f, yth, err);
		err_norm = gsl_blas_dnrm2(err);
		tolerance = (eps*gsl_blas_dnrm2(yth)+acc)*sqrt(*h/(b-a));
		if(err_norm < tolerance){
			//k++;
			*t += *h;
			gsl_blas_dcopy(yth, yt);
		}
		if(err_norm > 0.0){
			*h = *h*pow(tolerance/err_norm, 0.25)*0.95;
		}else{
			*h *=2;
		}
	}
	gsl_vector_free(yth);
	gsl_vector_free(err);
}
