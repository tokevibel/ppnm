#ifndef ODE_H

void rkstep45(double t, double h, gsl_vector* yt,
        void f(double t, gsl_vector* y, gsl_vector* dydt),
        gsl_vector* yth, gsl_vector* err);

void driver(double* t, double b, double* h, gsl_vector* yt, double acc,
        double eps, void stepper(
                double t, double h, gsl_vector* yt,
                void f(double t, gsl_vector* y, gsl_vector* dydt),
                gsl_vector* yth, gsl_vector* err),
        void f(double t, gsl_vector* y, gsl_vector* dydt));


#define ODE_H
#endif

