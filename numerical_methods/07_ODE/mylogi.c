#include<gsl/gsl_vector.h>
#include"ode.h"

void ode_logi(double t, gsl_vector* y, gsl_vector* dydt){
	double dydt0 = gsl_vector_get(dydt, 0);
	double y0 = gsl_vector_get(y, 0);
	dydt0 = y0*(1-y0);
	gsl_vector_set(dydt, 0, dydt0);
}

double mylogi(double x){
	if(x<0)return 1-mylogi(-x);
	if(x==0.0) return 0.5;
	double h =0.01;
	gsl_vector* yt = gsl_vector_alloc(1);
	gsl_vector_set(yt, 0, 0.5);
	double t=0;
	double acc = 1e-6;
	double eps = 1e-6;
	driver(&t, x, &h, yt, acc, eps, rkstep45, ode_logi);

	double result = gsl_vector_get(yt,0);
	gsl_vector_free(yt);

	return result;
}
