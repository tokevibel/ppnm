#include<math.h>
#include<stdio.h>


double mylogi(double x);

double logi(double x, double k){
	double y;
	y = 1/(1+exp(-k*x));
	return y;
}

double myorbit(double x);

int main(){
	printf("\nExercise ODE\n\n");
	printf("Part A\n");
	printf("A Runge-Kutta stepper rkstep45 and an adaptive-step-size\
 driver routine have been implemented and the routines are demonstrated\
 to work by solving the differential equation for the logistic\
 function and for planetary orbital motion.\n");

	double k=1;
	FILE* logi_data = fopen("logi.data", "w");
	for (double x = -3; x < 3; x += 0.1){
        	fprintf(logi_data, "%g %g %g\n", x, mylogi(x), logi(x,k));
	}
	fclose(logi_data);

	FILE* orbit_data = fopen("orbit.data", "w");
	for(double t = 0; t < 32*3.14; t += 0.1){
                fprintf(orbit_data, "%g %g\n", 1/myorbit(t)*cos(t), 1/myorbit(t)*sin(t));
	}
	fclose(orbit_data);

return 0;
}
