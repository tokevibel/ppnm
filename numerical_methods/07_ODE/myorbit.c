#include<gsl/gsl_vector.h>
#include"ode.h"

void ode_orbit(double t, gsl_vector* y, gsl_vector* dydt){
	gsl_vector_set(dydt, 0, gsl_vector_get(y, 1));
	double y0 = gsl_vector_get(y,0);
	double eps = 0.01;
	gsl_vector_set(dydt, 1, 1-y0+eps*y0*y0);
}

double myorbit(double x){
	double t = 0.0, h = 0.01, acc = 1e-5, eps = 1e-5;
	gsl_vector* yt = gsl_vector_alloc(2);
	gsl_vector_set(yt, 0, 1);
	gsl_vector_set(yt, 1, -0.5);
	driver(&t, x, &h, yt, acc, eps, rkstep45, ode_orbit);

	double result = gsl_vector_get(yt,0);
	gsl_vector_free(yt);

	return result;
}
