#include<stdio.h>
#include<math.h>

double mybessel_integ(double n, double x);

int main(){
	FILE* integ_data = fopen("mybessel_integ.data", "w");
	FILE* math_data = fopen("bessel_math.data", "w");
	for(double x = 0; x<20; x+=0.1){
		fprintf(integ_data, "%g", x);
		fprintf(math_data, "%g", x);
		for(int n = 0; n<3; n++){
			fprintf(integ_data, " %g", mybessel_integ(n, x));
			fprintf(math_data, " %g", jn(n,x));
		}
	fprintf(integ_data, "\n");
	fprintf(math_data, "\n");
	}
	fclose(integ_data);
	fclose(math_data);
return 0;
}
