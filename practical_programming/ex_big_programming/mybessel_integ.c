#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

struct input {double x; double n;};

double integrand(double t, void* params){
	struct input* input = (struct input*)params;
	double x = input->x;
	double n = input->n;
	return (1/M_PI)*cos(n*t-x*sin(t));
}

double mybessel_integ(double n, double x){
	struct input input = {x, n};
	int limit = 1000;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

	gsl_function F;
	F.function = integrand;
	F.params = (void*)&input;

	double result, error, epsabs=1e-8, epsrel=1e-8;

	int flag = gsl_integration_qag
		(&F, 0, M_PI, epsabs, epsrel, limit, 2, w, &result, &error);
	gsl_integration_workspace_free(w);

	if(flag!=GSL_SUCCESS) return NAN;
	return result;
}
