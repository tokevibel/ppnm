#include<stdio.h>
#include<gsl/gsl_sf_airy.h>

int main(){
	for(double x=-15;x<5;x+=0.01)
		printf("%g %g %g\n", x, gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE),\
		gsl_sf_airy_Bi(x, GSL_PREC_DOUBLE));
return 0;
}
