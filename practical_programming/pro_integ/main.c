#include<stdio.h>
//#include<math.h>

double integ();
double varmeth(double alp);

int main(){
	printf("\nIntegration of ln(x)/sqrt(x) from 0 to 1 is %g\n\n", integ());

	for(double alp=0.01; alp<2.0; alp+=0.01){
		fprintf(stderr, "%g %g\n", alp, varmeth(alp));
	}
return 0;
}
