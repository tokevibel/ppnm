#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double integrand(double x, void* params){
	return log(x)/sqrt(x);
}

double integ(){
	int limit = 100;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

	gsl_function F;
	F.function = integrand;
	F.params = NULL;

	double result, error, epsabs=1e-8, epsrel=1e-8;
	int flag = gsl_integration_qags
		(&F, 0, 1, epsabs, epsrel, limit, w, &result, &error);

	gsl_integration_workspace_free(w);

	if(flag!=GSL_SUCCESS) return NAN;
	return result;
}
