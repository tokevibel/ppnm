#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

struct alp{double alp;};

double integrand_num(double x, void* params){
	struct alp p = *(struct alp*)params;
	double alp = p.alp;
	double f = (-alp*alp*x*x/2 + alp/2 + x*x/2)*exp(-alp*x*x);
	return f;
}

double integrand_denom(double x, void* params){
	struct alp p = *(struct alp*)params;
        double alp = p.alp;
	double f = exp(-alp*x*x);
	return f;
}

double varmeth(double alp){
	int limit = 1000;

	gsl_integration_workspace* wn;
	wn = gsl_integration_workspace_alloc(limit);

	gsl_integration_workspace* wd;
	wd = gsl_integration_workspace_alloc(limit);

	struct alp params = {.alp=alp};

	gsl_function N;
	N.function = integrand_num;
	N.params = (void*)&params;

	gsl_function D;
	D.function = integrand_denom;
	D.params = (void*)&params;

	double resultn, resultd, errorn, errord, epsabs=1e-3, epsrel=1e-3;

	int flagn = gsl_integration_qagiu
		(&N, 0, epsabs, epsrel, limit, wn, &resultn, &errorn);

	int flagd = gsl_integration_qagiu
                (&D, 0, epsabs, epsrel, limit, wd, &resultd, &errord);

	gsl_integration_workspace_free(wn);
	gsl_integration_workspace_free(wd);

	if(flagn!=GSL_SUCCESS) return NAN;
	if(flagd!=GSL_SUCCESS) return NAN;

	return resultn/resultd;
}
