#include"komplex.h"
#include<stdio.h>

int main(){
	printf("\nTesting komlex_new and komplex_print by defining re(z)=3 and im(z)=4\n");
	komplex z = komplex_new(3, 4);
	komplex_print("z = ", z);

	printf("\nTesting komplex_set by setting re(z)=2 and im(z)=3\n");
	komplex_set(&z, 2, 3);
	komplex_print("z = ", z);

	printf("\nTesting komplex_add and komplex_sub by defining a new komplex number\n");
	komplex a = komplex_new(-1, 2);
	komplex_print("a = ", a);
	komplex sum = komplex_add(z, a);
	komplex SUM = komplex_new(1, 5);
	komplex_print("z+a should  = ", SUM);
	komplex_print("z+a actually = ", sum);
	komplex diff = komplex_sub(z, a);
	komplex DIFF = komplex_new(3, 1);
	komplex_print("z+a should  = ", DIFF);
	komplex_print("z+a actually = ", diff);
}
