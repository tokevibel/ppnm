#include<limits.h>
#include<float.h>
#include<stdio.h>

int equal(double a, double b, double tau, double epsilon);

int main(){
	//exercise 1.i
	//while
	int i=1;
	while(i+1>i) {i++;}
	printf("\nExercise 1.i\n\n");
	printf("From limits.h INT_MAX = %i\nwhich should be equal to, what is obtained with looping:\n", INT_MAX);
	printf("while: my max int = %i\n",i);
	//for
	int i2=1;
	for (int i=1; i+1>i2; ++i){++i2;}
	printf("for: my max int = %i\n",i2);
	//do while
	int i3=1;
	do {++i3;}
	while(i3+1>i3);
	printf("do while: my max int = %i\n",i3);

	//exercise 1.ii
	//while
	int i4=-1;
	while(i4-1<i4) {i4--;}
	printf("\nExercise 1.ii\n\n");
	printf("From limits.h INT_MIN = %i\nwhich should be equal to, what is obtained with looping:\n", INT_MIN);
	printf("while: my min int = %i\n",i4);

	//for
	int i5=-1;
	for (int i=-1; i-1<i5; --i){--i5;}
	printf("for: my min int = %i\n",i5);

	//do while
	int i6=-1;
	do {--i6;}
	while(i6-1<i6);
	printf("do while: my min int = %i\n",i6);

	//exercise 1.iii
	//While
	float x1=1;
	while(1+x1!=1){x1/=2;}
	x1*=2;
	printf("\nExercise 1.iii\n\nUsing while loops\n");
	printf("Float: Machine epsilon = %g\n",x1);
	printf("FLT_EPSILON = %g\n", FLT_EPSILON);

	double x2=1;
	while(1+x2!=1){x2/=2;}
	x2*=2;
	printf("Double: Machine epsilon = %g\n",x2);
	printf("DBL_EPSILON = %g\n", DBL_EPSILON);

	long double x3=1;
	while(1+x3!=1){x3/=2;}
	x3*=2;
	printf("Long double: Machine epsilon = %Lg\n",x3);
	printf("LDBL_EPSILON = %Lg\n", LDBL_EPSILON);

	//Do While
	float f1=1;
	do{f1/=2;}
	while(1+f1!=1);
	f1*=2;
	printf("\nUsing do while loops\n");
	printf("Float: Machine epsilon = %g\n",f1);
	printf("FLT_EPSILON = %g\n", FLT_EPSILON);

	double f2=1;
	do{f2/=2;}
	while(1+f2!=1);
	f2*=2;
	printf("Double: Machine epsilon = %g\n",f2);
	printf("DBL_EPSILON = %g\n", DBL_EPSILON);

	long double f3=1;
	do{f3/=2;}
	while(1+f3!=1);
	f3*=2;
	printf("Long double: Machine epsilon = %Lg\n",f3);
	printf("LDBL_EPSILON = %Lg\n", LDBL_EPSILON);

	//for
	float e1;
	for(e1=1; 1+e1!=1; e1/=2){}
	e1*=2;
	printf("\nUsing for loops\n");
	printf("Float: Machine epsilon = %g\n",	e1);
	printf("FLT_EPSILON = %g\n", FLT_EPSILON);

	double e2;
	for(e2=1; 1+e2!=1; e2/=2){}
	e2*=2;
	printf("Double: Machine epsilon = %g\n",e2);
	printf("DBL_EPSILON = %g\n", DBL_EPSILON);

	long double e3;
	for(e3=1; 1+e3!=1; e3/=2){}
	e3*=2;
	printf("Long double: Machine epsilon = %Lg\n",e3);
	printf("LDBL_EPSILON = %Lg\n", LDBL_EPSILON);

	//Exercise 2.i
	int max=INT_MAX/3;
	float f = 1.0;
	float sum_up_float = 0;
	for(int i=1; i!=max+1; ++i)
		{
		sum_up_float += f/i;
 		}
	printf("\n\nExercise 2.i\n");
	printf("sum_up_float = %g\n",sum_up_float);
	float sum_down_float = 0;
	for(int i=max; i!=0; --i)
                {
                sum_down_float += f/i;
                }
	printf("sum_down_float = %g\n", sum_down_float);

	//Exercise 2.ii
	printf("\nExercise 2.ii\nThese two sums are not identical, as they should be. This is caused by the limited precision of the representation of numbers on a computer. When we conduct the sum_up_float we will eventually add very small numbers to numbers of order 10. When the small numbers are less than half the difference between the big number and its next representable number it will not contribute to sum. This problem does not occur when we perfon the sum_up_float and this number is therefore both higher and more correct.\n");

	//Exercise 2.iii
	printf("\nExercise 2.iii\nThe sum should not converge, but would do anyway if we calculate the sum_up_float and also when we calculate sum_down_float when max = INT_MAX.\n");

	//Exercise 2.iv
	printf("\nExercise 2.iv\n");
	double g = 1.0;
	double sum_up_double = 0;
	//printf("%.16g\n", g/max);
	for(int i=1; i!=max+1; ++i)
		{
		sum_up_double += g/i;
 		}
	printf("sum_up_double = %.16g\n", sum_up_double);
	double sum_down_double = 0;
	for(int i=max; i!=0; --i)
                {
                sum_down_double += g/i;
                }
	printf("sum_down_double = %.16g\n", sum_down_double);
	//printf("%.16g\n",sum_down_double + g/max);
	printf("This time the sums are nearly equal, since the double precision is nearly high enough for 1/max to be properly added to a number of order 10.\n");

	//Exercise 3
        printf("\nExercise 3\n\n");
	printf("Testing if a=2.0 and b=8.0 is equal to each other with absolut and relative precision A) 1.0 and B) 10.0\n");
	double a = 2.0;
	double b = 8.0;
	double tau = 1.0;
	double epsilon = 1.0;
	int x = equal(a, b, tau, epsilon);
	if (x==1)
		printf("A) They are equal to the given precision.\n");
	if (x==0)
		printf("A) They are not equal.\n");
	tau = 10;
		epsilon = 10;
	x = equal(a, b, tau, epsilon);
	if (x==1)
		printf("B) They are equal to the given precision.\n");
	if (x==0)
		printf("B) They are not equal.\n");
return 0;
}
