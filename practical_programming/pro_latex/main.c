#include<stdio.h>
#include<stdlib.h>

double error(double t);

int main(int argc, char** argv){
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double dx = atof(argv[3]);
	for(double x = a; x<=b+dx; x+=dx){
		double error_value = error(x);
		printf("%g %g\n", x, error_value);
	}
return 0;
}
