#include<stdio.h>
#include<math.h>
#include<stdlib.h>

double mylogi(double x, double k);
double myorbit(double t, double eps, double y0_start, double y1_start);

double logi(double x, double k){
	double y;
	y = 1/(1+exp(-k*x));
	return y;
}

int main(int argc, char** argv){
	int n = atoi(argv[1]);
	if (n == 1){
		double k=1;
		for (double x = -3; x < 3; x += 0.1)
		fprintf(stdout, "%g %g %g\n", x, mylogi(x,k), logi(x,k));
		}
	if (n==2){
		double eps = atof(argv[2]);
		double y0 = atof(argv[3]);
		double y1 = atof(argv[4]);
		for(double t = 0; t < 32*3.14; t += 0.1)
		fprintf(stdout, "%g %g\n", (1/myorbit(t,eps,y0,y1))*cos(t), (1/myorbit(t,eps,y0,y1))*sin(t));
		}
	return 0;
}
