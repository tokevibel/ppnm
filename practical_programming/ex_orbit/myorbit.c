#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<stdlib.h>

int ode_orbit(double t, const double y[], double dydt[],  void *params){

	double eps = *(double*)params;
	dydt[0] = y[1];
	dydt[1] = 1-y[0]+eps*y[0]*y[0];
	return GSL_SUCCESS;
}

double myorbit(double t, double eps, double y0_start, double y1_start){
	gsl_odeiv2_system sys;
	sys.function = ode_orbit;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&eps;

	gsl_odeiv2_driver *driver;
	double hstart = 0.01, abs = 1e-5, eps_er = 1e-5;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys,
					       gsl_odeiv2_step_rkf45,
					       hstart, abs, eps_er);

	double t0 = 0;
	double y[] = { y0_start , y1_start };
	gsl_odeiv2_driver_apply(driver, &t0, t, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
