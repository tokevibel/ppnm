#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<stdlib.h>

int ode_logi(double t, const double y[], double dydt[], void* params){
	double k=*(double*)params;
	dydt[0] = k*y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

double mylogi(double t, double k){
	if(t<0) return 1-mylogi(-t,k);
	gsl_odeiv2_system sys;
	sys.function = ode_logi;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = (void*)&k;

	gsl_odeiv2_driver *driver;
	double hstart = 0.001, abs = 1e-5, eps = 1e-5;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys,
					       gsl_odeiv2_step_rkf45,
					       hstart, abs, eps);

	double t0 = 0;
	double y[] = { 0.5 };
	gsl_odeiv2_driver_apply(driver, &t0, t, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
