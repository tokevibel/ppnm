#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int ode_hydro(double t, const double y[], double dydt[],  void *params){

	double eps = *(double*)params;
	dydt[0] = y[1];
	dydt[1] = -2*(1/t+eps)*y[0];
	return GSL_SUCCESS;
}

double 	myhydro(double rmax, double eps){
	gsl_odeiv2_system sys;
	sys.function = ode_hydro;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&eps;

	gsl_odeiv2_driver *driver;
	double hstart = 0.01, epsabs = 1e-12, epsrel = 1e-12;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys,
					       gsl_odeiv2_step_rkf45,
					       hstart, epsabs, epsrel);
	double rmin = 1e-3;
	double t0 = rmin;
	double t = rmax;
	double y[] = { rmin-rmin*rmin , 1-2*rmin };
	gsl_odeiv2_driver_apply(driver, &t0, t, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
