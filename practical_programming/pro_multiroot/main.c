#include<stdio.h>
#include<math.h>


double myhydro(double t, double eps);
double eps_root(double rmax, double eps_init);

int main(){
	double rmax = 10;
	double eps_init = -0.45;
	double eps = eps_root(rmax, eps_init);
	fprintf(stdout,"The energy of the S-wave with r_max = %g is %g\n", rmax, eps);
	for (double x = 0.01; x<rmax+0.5; x+=0.01)
		fprintf(stderr,"%g %g %g\n", x, myhydro(x, eps), x*exp(-x));
return 0;
}
