#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
//#include<math.h>
#include<gsl/gsl_vector.h>

double myhydro(double rmax, double eps);

int aux_eq(const gsl_vector* x, void* params, gsl_vector* f){

	double rmax = *(double*)params;
	const double eps = gsl_vector_get(x, 0);
	const double y0 = myhydro(rmax, eps);
	gsl_vector_set(f, 0, y0);

	return GSL_SUCCESS;
}

double eps_root(double rmax, double eps_init){
	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;

	int status;
  	size_t iter = 0;

	const size_t n = 1;
	gsl_multiroot_function f = {&aux_eq, n, (void*)&rmax};

  	gsl_vector *start = gsl_vector_alloc(n);

  	gsl_vector_set(start, 0, eps_init);

	T = gsl_multiroot_fsolver_hybrids;
	s = gsl_multiroot_fsolver_alloc(T, n);
	gsl_multiroot_fsolver_set(s, &f, start);

	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate (s);

      		if (status)   /* check if solver is stuck */
        		break;

      		status = gsl_multiroot_test_residual(s->f, 1e-12);
    	}while(status == GSL_CONTINUE && iter < 1000);

	printf("status = %s\n", gsl_strerror(status));

	printf("Number of iterations: %li\n", iter);

	double eps0 = gsl_vector_get(s->x,0);

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(start);
	return eps0;
}

