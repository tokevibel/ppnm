#include<math.h>
#include<stdio.h>
#include<complex.h>

int main() {
	double x1 = 5;
	double y1 = tgamma(x1);
	printf("gamma(5) = %g\n", y1);

	double x2 = 0.5;
	double y2 = j1(x2);
	printf("J_1(0.5) = %g\n",y2);

	double complex x3 = -2;
	double complex y3 = csqrt(x3);
	printf("sqrt(-2) = %g+I*%g\n", creal(y3), cimag(y3));

	double complex y4 = cpow(M_E, I);
	printf("e^I = %g+I*%g\n", creal(y4), cimag(y4));

	double complex y5 = cpow(M_E, I*M_PI);
        printf("e^(I*Pi) = %g+I*%g\n", creal(y5), cimag(y5));

	double complex y6 = cpow(I, M_E);
        printf("I^e = %g+I*%g\n\n", creal(y6), cimag(y6));

	float a = 0.1111111111111111111111111111;
	double b = 0.1111111111111111111111111111;
	long double c = 0.1111111111111111111111111111L ;
	printf("In float, double, and long double precision 0.1111111111111111111111111111 is equal to:\n");  
	printf("%.25g\n", a);
	printf("%.25lg\n", b);
	printf("%.25Lg\n",c);
return 0;
}
