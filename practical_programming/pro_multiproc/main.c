#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<math.h>

struct input{int N; int Nin; unsigned int seed;};

void* dart(void* arg){ /* a function to run in a separate thread */
	struct input* input = (struct input*)arg;
	for(int i=0;i< input->N;++i){
		double x  = (double)rand_r(&(input->seed))/(double)RAND_MAX-0.5;
		double y  = (double)rand_r(&(input->seed))/(double)RAND_MAX-0.5;
		if(y*y+x*x<=0.25)
			input->Nin += 1;
	}
	return NULL;
}

double Pthread(int Ntot){
	pthread_t thread;
	struct input input[2];
        input[0].N = Ntot/2;
        input[0].Nin =  0;
	input[0].seed =  1;
	input[1].N = Ntot/2;
	input[1].Nin =  0;
	input[1].seed =  2;
        pthread_create( /* create a thread */
                &thread, NULL,
                dart, (void*)&input[1]); /* and run bar(x) in it */
        dart((void*)&input[0]); /* meanwhile in the master thread... */
        pthread_join(thread, NULL); /* join the other thread */
        int Nin = input[0].Nin + input[1].Nin;

	return  4*(double)Nin/(double)Ntot;
}
int main(int argc, char** argv){
	//pthreads
	int Ntot = argc>1? (int)atof(argv[1]) : (int)1e6;
	double pi = Pthread(Ntot);
	printf("\n############\nPthreads\n############\n\n");
	printf("For N = %.1e pi is calculated to be %6.7g\n\n",(double)Ntot, pi);

	//OpenMP
	int  Ntot2 = argc>2? (int)atof(argv[2]):(int)1e6;
	struct input input_omp[2];
	for(int i = 0; i<2; i++){
		input_omp[i].N=Ntot2/2;
		input_omp[i].Nin=0;
		input_omp[i].seed=i+1;
	}
	#pragma omp parallel for
	for(int i = 0; i<2;i++){
		dart((void*)&input_omp[i]);
	}
	int Nin2= input_omp[0].Nin + input_omp[1].Nin;
	double pi2 = 4*(double)Nin2/(double)Ntot2;
	printf("\n############\nOpenMP\n############\n\n");
        printf("For N = %.1e pi is calculated to be %6.7g\n\n", (double)Ntot2, pi2);

	return 0;
}
