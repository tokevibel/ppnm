#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

double rosenbrock(const gsl_vector *v, void *params){
	double x, y;
	double *p = (double*)params;

	x = gsl_vector_get(v, 0);
	y = gsl_vector_get(v, 1);

	return (p[0]-x)*(p[0]-x) + p[1]*(y-x*x)*(y-x*x);
}

void rosen_min(void){
	double par[2] = {1.0, 100.0};

	const gsl_multimin_fminimizer_type *T =
		gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *s = NULL;
	gsl_vector *ss, *x;
	gsl_multimin_function minex_func;

	size_t iter = 0;
	int status;
	double size;

  	/* Starting point */
 	x = gsl_vector_alloc (2);
  	gsl_vector_set (x, 0, 0.0);
  	gsl_vector_set (x, 1, 0.0);

	/* Set initial step sizes to 1 */
	ss = gsl_vector_alloc (2);
	gsl_vector_set_all (ss, 1.0);

	/* Initialize method and iterate */
	minex_func.n = 2;
	minex_func.f = rosenbrock;
	minex_func.params = par;

	s = gsl_multimin_fminimizer_alloc (T, 2);
	gsl_multimin_fminimizer_set (s, &minex_func, x, ss);

	do{
      		iter++;
		status = gsl_multimin_fminimizer_iterate(s);

		if (status)
        		break;

		size = gsl_multimin_fminimizer_size (s);
		status = gsl_multimin_test_size (size, 1e-8);

		fprintf (stdout, "%5ld %10.4e %10.4e f() = %7.3g size = %.3g\n",
			iter,
			gsl_vector_get (s->x, 0),
			gsl_vector_get (s->x, 1),
			s->fval, size);
	}
	while (status == GSL_CONTINUE && iter < 100);

	printf ("Converged to minimum at (x, y) = (%12.10g, %12.10g) after %li iterations \n",
		gsl_vector_get (s->x,0), gsl_vector_get (s->x,1), iter);
	gsl_vector_free(x);
	gsl_vector_free(ss);
	gsl_multimin_fminimizer_free(s);

}
