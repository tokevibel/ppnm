void fit(int n, double* t, double* y, double* e);
void rosen_min(void);

int main(){
	// 1. Minimum of Rosenbrock function
	rosen_min();

	// 2. Least-square fit
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);

	fit(n, t, y, e);
	return 0;
}
