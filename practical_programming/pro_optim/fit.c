#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<math.h>

struct expdata {int n; double *t, *y, *e;};

double master(const gsl_vector *x, void *params) {
	double  A = gsl_vector_get(x, 0);
	double  T = gsl_vector_get(x, 1);
	double  B = gsl_vector_get(x, 2);
	struct expdata *p = (struct expdata*)params;
	int     n = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum=0;
	#define f(t) A*exp(-(t)/T) + B
	for(int i=0; i<n; i++) sum+= pow( (f(t[i]) - y[i])/e[i], 2);
	return sum;
}

void fit(int n, double* t, double* y, double* e){
	int dim=3;
	struct expdata data;
	data.n=n;
	data.t=t;
	data.y=y;
	data.e=e;
	gsl_multimin_function F;
	F.f = master;
	F.n = dim;
	F.params=(void*)&data;

	gsl_multimin_fminimizer *M;
	#define TYPE gsl_multimin_fminimizer_nmsimplex2
	M = gsl_multimin_fminimizer_alloc(TYPE,dim);
	gsl_vector* start=gsl_vector_alloc(dim);
	gsl_vector* step=gsl_vector_alloc(dim);
	gsl_vector_set(start,0,1);
	gsl_vector_set(start,1,1);
	gsl_vector_set(start,2,1);
	gsl_vector_set(step,0,0.1);
	gsl_vector_set(step,1,0.1);
	gsl_vector_set(step,2,0.1);

	gsl_multimin_fminimizer_set(M,&F,start,step);

	int iter=0,status;
	double size;
	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(M);
		if (status) break;

		size = gsl_multimin_fminimizer_size (M);
		status = gsl_multimin_test_size (size, 1e-6);

		if (status == GSL_SUCCESS){
	          	printf ("converged to minimum at\n");
        		}

		/*fprintf (stderr,"iter=%5d A=%g T=%g B=%g master=%g size=%g\n",
              		iter,
              		gsl_vector_get (M->x, 0),
              		gsl_vector_get (M->x, 1),
              		gsl_vector_get (M->x, 2),
              		M->fval, size);
		*/
	}while (status == GSL_CONTINUE && iter < 100);

	double A=gsl_vector_get(M->x,0);
	double T=gsl_vector_get(M->x,1);
	double B=gsl_vector_get(M->x,2);
	for(int i=0;i<n;i++)
		fprintf(stderr,"%g %g %g %g\n",
		*(t+i),*(y+i),*(e+i)
		,A*exp(-*(t+i)/T)+B
		);
	FILE* result = fopen("fit_result.txt", "w");
	fprintf(result, "A = %g, T = %g, B =%g\n", A, T, B);
	fclose(result);

	gsl_multimin_fminimizer_free(M);
	gsl_vector_free(start);
	gsl_vector_free(step);
}
